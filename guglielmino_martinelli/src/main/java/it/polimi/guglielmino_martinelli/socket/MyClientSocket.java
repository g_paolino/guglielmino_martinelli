package it.polimi.guglielmino_martinelli.socket;

import it.polimi.guglielmino_martinelli.Main;
import it.polimi.guglielmino_martinelli.events.GenericEvent;
import it.polimi.guglielmino_martinelli.view.GameView;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;

/**
 * Classe client socket.
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public class MyClientSocket implements Observer {

	private GameView gameView;
	private Socket socket;
	private int myIndex;

	public MyClientSocket(String name, String ipAddressServer, int port){

		//Creo la view di gioco del client.
		this.gameView = new GameView();
		gameView.addObserver(this);

		try {
			this.socket = new Socket(ipAddressServer, port);
			contactServer(name);
		} catch (IOException e) {
			Main.LOGGER.log(Level.SEVERE, "Errore", e.getMessage());
		}

		//Lancio il thread che si mette in attesa di comunicazioni da parte del server.
		(new Thread(new Runnable(){

			@Override
			public void run() {
				try {
					waitForMessages();
				} catch (IOException | ClassNotFoundException e) {
					Main.LOGGER.log(Level.SEVERE, "Errore", e.getMessage());
				}
			}

		})).start();
	}

	/**
	 * Procedura di primo contatto al server (mando il nome del giocatore che usa questo client).
	 * @param name Nome del giocatore.
	 * @throws IOException
	 */
	private void contactServer(String name) throws IOException{
		ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
		oos.writeObject(name);
		oos.flush();
	}

	/**
	 * Ciclo che si mette in attesa di comunicazioni da parte del server, i messaggi vengono inoltrati alla View.
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	private void waitForMessages() throws IOException, ClassNotFoundException{

		ObjectInputStream in;

		while(true){

			in = new ObjectInputStream(socket.getInputStream());
			Object arg = in.readObject();

			if(arg!=null){
				if(arg instanceof Integer){
					myIndex = ((int)arg);
				}else{
					gameView.update(null, (GenericEvent)arg);
				}
			}
		}
	}

	/**
	 * Intercetta gli eventi della view e li inoltra al server.
	 */
	@Override
	public void update(Observable o, Object arg) {

		((GenericEvent)arg).setPlayerIndex(myIndex);
		try {
			ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
			oos.writeObject(arg);
			oos.flush();
		} catch (IOException e) {
			Main.LOGGER.log(Level.SEVERE, "Errore", e.getMessage());
		}	
	}
}