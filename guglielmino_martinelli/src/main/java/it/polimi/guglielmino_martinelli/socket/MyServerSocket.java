package it.polimi.guglielmino_martinelli.socket;

import it.polimi.guglielmino_martinelli.Main;
import it.polimi.guglielmino_martinelli.events.GenericEvent;
import it.polimi.guglielmino_martinelli.events.PlayerNameInsertedEvent;
import it.polimi.guglielmino_martinelli.view.ViewImplementationServerSocket;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Inet4Address;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;

/**
 * Classe server socket
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public class MyServerSocket {

	private Map<Integer, Socket> clients;

	private int index;

	private int numberOfPlayers;

	private ServerSocket server;

	private ViewImplementationServerSocket viewImplementationServerSocket;

	public MyServerSocket(int numberOfPlayers){
		this.clients = new HashMap<Integer, Socket>();
		this.index = 1;
		this.numberOfPlayers = numberOfPlayers;
		this.viewImplementationServerSocket = new ViewImplementationServerSocket(numberOfPlayers);
	}

	/**
	 * Procedura di avvio del server socket.
	 * @param firstPlayerName Nome del primo giocatore (quello che ospita il server).
	 * @param port Numero di porta usato dal server.
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public void avviaServerSocket(final String firstPlayerName, final int port) throws IOException, ClassNotFoundException{

		//Apro la socket.
		server = new ServerSocket(port);

		viewImplementationServerSocket.setServer(this);

		//Avvio il client del giocatore che ospita il server.
		new Thread(new Runnable(){
			@Override
			public void run() {
				try {
					new MyClientSocket(firstPlayerName, Inet4Address.getLocalHost().getHostAddress(), port);
				} catch (UnknownHostException e) {
					Main.LOGGER.log(Level.SEVERE, "Errore", e.getMessage());
				}
			}
		}).start();

		Socket s;
		String name;

		//Attendo l'arrivo dei client.
		while (index <= numberOfPlayers){

			//Accetto la socket del client.
			s = server.accept();

			//Aggiungo la socket del client all'hashmap con tutte le altre.
			clients.put(index, s);

			ObjectInputStream in;
			//Attendo la comunicazione del nome del giocatore.
			while(true){
				in = (new ObjectInputStream(s.getInputStream()));
				name = ((String)in.readObject());

				if(name!=null){
					PlayerNameInsertedEvent event = new PlayerNameInsertedEvent(name);
					event.setPlayerIndex(index);
					viewImplementationServerSocket.sendEvent(event);
					break;
				}
			}

			//Preparo l'indice per il prossimo client che si connetterà.
			index++;
		}

		//Trasmetto ai client i loro indici assegnati dal server.
		for(Entry<Integer, Socket> entryClient : clients.entrySet()){
			ObjectOutputStream oos = new ObjectOutputStream(entryClient.getValue().getOutputStream());
			oos.writeObject(entryClient.getKey());
			oos.flush();
		}

		//Faccio partire i thread di ascolto dei client.
		listenClients();
	}

	/**
	 * Procedura di inoltro di un evento verso uno specifico client.
	 * @param event Evento da inoltrare.
	 * @param clientIndex Indice del client che riceverà il messaggio.
	 * @throws IOException
	 */
	public void sendEventToClient(GenericEvent event, int clientIndex) throws IOException{
		for(Entry<Integer, Socket> entryClient : clients.entrySet()){
			if(entryClient.getKey()==clientIndex){
				ObjectOutputStream oos = new ObjectOutputStream(entryClient.getValue().getOutputStream());
				oos.writeObject(event);
				oos.flush();
			}
		}
	}

	/**
	 * Procedura di inoltro di un evento in broadcast verso tutti i client.
	 * @param event Evento da inoltrare.
	 * @throws IOException
	 */
	public void sendBroadcastEvent(GenericEvent event) throws IOException{
		for(Socket client : clients.values()){
			ObjectOutputStream oos = new ObjectOutputStream(client.getOutputStream());
			oos.writeObject(event);
			oos.flush();
		}
	}

	/**
	 * Procedura che crea un thread di ascolto per ogni client connesso.
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	private void listenClients() throws IOException, ClassNotFoundException{
		for(final Socket client : clients.values()){
			new Thread(new Runnable(){
				@Override
				public void run() {
					try {
						waitForMessages(client);
					} catch (IOException | ClassNotFoundException e) {
						Main.LOGGER.log(Level.SEVERE, "Errore", e.getMessage());
					}
				}
			}).start();
		}
	}

	/**
	 * Ciclo di ascolto dei messaggi provenienti da un client.
	 * @param client Socket del client che si sta ascoltando.
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	private void waitForMessages(Socket client) throws IOException, ClassNotFoundException{
		while(true){
			ObjectInputStream ois = new ObjectInputStream(client.getInputStream());
			viewImplementationServerSocket.sendEvent((GenericEvent)ois.readObject());
		}
	}
}