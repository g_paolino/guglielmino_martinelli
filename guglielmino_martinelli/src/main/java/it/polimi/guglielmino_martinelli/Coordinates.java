package it.polimi.guglielmino_martinelli;

import java.io.Serializable;

/**
 * Classe di coordinate, utilizzata per il posizionamento di oggetti sulla view.
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public class Coordinates implements Serializable{

	private static final long serialVersionUID = 545765492597363999L;
	/**
	 * Variabile che rappresenta l'ascissa.
	 */
	private int x;
	/**
	 * Variabile che rappresenta l'ordinata.
	 */
	private int y;

	public Coordinates(){
	}

	public Coordinates(int x, int y){
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	@Override
	public String toString() {
		return "Coordinates [x=" + x + ", y=" + y + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if(this.x == ((Coordinates)obj).getX() && this.y == ((Coordinates)obj).getY()){
			return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}
}