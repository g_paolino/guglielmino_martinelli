package it.polimi.guglielmino_martinelli.events;

import java.io.Serializable;

/**
 * Evento generico che può essere passato da un oggetto osservato al suo osservatore.
 * @author Paolo Guglielmino
 * @author Simone  Martinelli
 */
public abstract class GenericEvent implements Serializable {

	private static final long serialVersionUID = 775360413727919204L;
	/**
	 * Tipo dell'evento.
	 */
	private EventType type;

	private int playerIndex;

	public GenericEvent(EventType type) {
		this.type = type;
		this.playerIndex = 0;
	}

	public EventType getType() {
		return type;
	}

	public int getPlayerIndex() {
		return playerIndex;
	}

	public void setPlayerIndex(int playerIndex) {
		this.playerIndex = playerIndex;
	}

	/**
	 * Enum contenente tutti i tipi di eventi.
	 * @author Paolo Guglielmino
	 * @author Simone Martinelli
	 */
	public enum EventType {
		ADD_REGION_EVENT,
		ADD_ROAD_EVENT,
		ASK_WHICH_SHEEP_EVENT,
		CARDS_CHANGED_EVENT,
		CHANGE_TURN_EVENT,
		CHOSED_BUY_CARD_EVENT,
		CHOSED_CARD_EVENT,
		CHOSED_MOVE_SHEEP_EVENT,
		CHOSED_MOVE_SHEPERD_EVENT,
		CHOSED_ROAD_EVENT,
		CHOSED_REGION_EVENT,
		CHOSED_SHEEP_TO_MOVE_EVENT,
		CHOSED_SHEPERD_EVENT,
		CLIENT_DISCONNECTED_EVENT,
		ENABLE_BUY_CARD_EVENT,
		ENABLE_MOVE_SHEEP_EVENT,
		FINAL_SCREEN_EVENT,
		GENERIC_EVENT,
		HIDE_WHITE_SHEEP_EVENT,
		INITIAL_COSTS_SETTED_EVENT,
		INITIAL_PHASE_EVENT,
		MOVE_BLACK_SHEEP_EVENT,
		MOVE_WHITE_SHEEP_EVENT,
		OCCUPIED_ROAD_EVENT,
		PLACE_FENCES_EVENT,
		PLAYER_CHANGED_EVENT,
		PLAYER_NAME_INSERTED_EVENT,
		PLAYER_NAME_SETTED_EVENT,
		PLAYERS_NAMES_INSERTED_EVENT,
		PLAYERS_NAMES_SETTED_EVENT,
		PLAYERS_READY_EVENT,
		SHEPERD_PLACED_EVENT,
		UPDATE_CARDS_CURRENT_COST_EVENT,
		UPDATE_DICE_RESULT_EVENT,
		UPDATE_INSTRUCTION_EVENT,
		UPDATE_MONEY_EVENT,
		WANTS_TO_MOVE_BLACK_EVENT;
	}
}