package it.polimi.guglielmino_martinelli.events;


/**
 * Evento lanciato quando il nome del giocatore viene settato nel model (gioco in rete).
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public class PlayerNameSettedEvent extends GenericEvent {

	private static final long serialVersionUID = 9160639134684105024L;
	/**
	 * Nome del giocatore.
	 */
	private String name;

	public PlayerNameSettedEvent(String name) {
		super(EventType.PLAYER_NAME_SETTED_EVENT);
		this.name = name;
	}

	public String getName() {
		return name;
	}
}