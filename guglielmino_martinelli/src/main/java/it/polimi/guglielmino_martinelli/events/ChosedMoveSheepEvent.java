package it.polimi.guglielmino_martinelli.events;

/**
 * Evento di scelta mossa muovi pecora.
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public class ChosedMoveSheepEvent extends GenericEvent{

	private static final long serialVersionUID = 657213848745655309L;

	public ChosedMoveSheepEvent(){
		super(EventType.CHOSED_MOVE_SHEEP_EVENT);
	}
}