package it.polimi.guglielmino_martinelli.events;

public class EnableMoveSheepEvent extends GenericEvent{

	private static final long serialVersionUID = -6555691606146499128L;
	private boolean enable;
	
	public EnableMoveSheepEvent(boolean enable) {
		super(EventType.ENABLE_MOVE_SHEEP_EVENT);
		
		this.enable = enable;
	}

	public boolean isEnable() {
		return enable;
	}
	
}
