package it.polimi.guglielmino_martinelli.events;

/**
 * Evento di scelta mossa muovi pastore.
 * @author Paolo Guglielmino.
 * @author Simone Martinelli.
 */
public class ChosedMoveSheperdEvent extends GenericEvent{

	private static final long serialVersionUID = -3478461430268499283L;

	public ChosedMoveSheperdEvent(){
		super(EventType.CHOSED_MOVE_SHEPERD_EVENT);
	}
}