package it.polimi.guglielmino_martinelli.events;

public class ChosedBuyCardEvent extends GenericEvent {
	
	private static final long serialVersionUID = 2440733080860825753L;

	public ChosedBuyCardEvent() {
		super(EventType.CHOSED_BUY_CARD_EVENT);
	}
}
