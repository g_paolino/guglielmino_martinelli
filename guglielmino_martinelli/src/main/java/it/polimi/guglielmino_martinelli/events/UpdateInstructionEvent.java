package it.polimi.guglielmino_martinelli.events;


/**
 * Evento di modifica dell'istruzione che viene visualizzata nella GameView di un giocatore.
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public class UpdateInstructionEvent extends GenericEvent{

	private static final long serialVersionUID = -7072814131191970650L;
	/**
	 * Testo dell'istruzione.
	 */
	private String instruction;
	/**
	 * Giocatore a cui modificare l'istruzione.
	 */
	private int playerIndex;

	public UpdateInstructionEvent(String instruction, int playerIndex){
		super(EventType.UPDATE_INSTRUCTION_EVENT);
		this.instruction = instruction;
		this.playerIndex = playerIndex;
	}

	public String getInstruction(){
		return instruction;
	}

	public int getPlayerIndex(){
		return playerIndex;
	}
}