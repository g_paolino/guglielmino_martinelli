package it.polimi.guglielmino_martinelli.events;

import it.polimi.guglielmino_martinelli.Coordinates;

/**
 * Evento generato al momento di nascondere una delle pecore bianche.
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public class HideWhiteSheepEvent extends GenericEvent{

	private static final long serialVersionUID = 4149257464405193077L;
	private Coordinates xy;

	public HideWhiteSheepEvent(Coordinates xy){
		super(EventType.HIDE_WHITE_SHEEP_EVENT);
		this.xy = xy;
	}

	public Coordinates getXY(){
		return xy;
	}
}