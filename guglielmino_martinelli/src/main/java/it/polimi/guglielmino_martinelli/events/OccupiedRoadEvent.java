package it.polimi.guglielmino_martinelli.events;

import it.polimi.guglielmino_martinelli.Coordinates;

/**
 * Evento generato quando una strada viene settata busy.
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public class OccupiedRoadEvent extends GenericEvent{

	private static final long serialVersionUID = 5470115579633407267L;
	/**
	 * Strada che è stata occupata.
	 */
	private Coordinates xy;

	private int sheperdIndex;

	public OccupiedRoadEvent(Coordinates xy, int sheperdIndex){
		super(EventType.OCCUPIED_ROAD_EVENT);
		this.xy=xy;
		this.sheperdIndex=sheperdIndex;
	}

	public Coordinates getXY(){
		return xy;
	}

	public int getSheperdIndex(){
		return sheperdIndex;
	}
}