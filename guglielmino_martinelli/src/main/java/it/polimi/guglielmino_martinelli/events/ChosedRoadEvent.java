package it.polimi.guglielmino_martinelli.events;

import it.polimi.guglielmino_martinelli.Coordinates;

/**
 * Evento generato al click su una strada.
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public class ChosedRoadEvent extends GenericEvent{

	private static final long serialVersionUID = -6326659285587417454L;
	/**
	 * Strada scelta.
	 */
	private Coordinates xy;

	public ChosedRoadEvent(Coordinates xy){
		super(EventType.CHOSED_ROAD_EVENT);
		this.xy = xy;
	}

	public Coordinates getXY() {
		return xy;
	}
}