package it.polimi.guglielmino_martinelli.events;

/**
 * Evento lanciato quando un giocatore ha inserito il suo nome (gioco in rete).
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public class PlayerNameInsertedEvent extends GenericEvent {

	private static final long serialVersionUID = -3055211625270462455L;
	/**
	 * Nome del giocatore.
	 */
	private String name;

	public PlayerNameInsertedEvent(String name) {
		super(EventType.PLAYER_NAME_INSERTED_EVENT);
		this.name = name;
	}

	public String getName() {
		return name;
	}
}