package it.polimi.guglielmino_martinelli.events;

public class WantsToMoveBlackEvent extends GenericEvent{

	private static final long serialVersionUID = 5367901754080060878L;
	private boolean wantsToMoveBlack;
	
	public WantsToMoveBlackEvent(boolean wantsToMoveBlack) {
		super(EventType.WANTS_TO_MOVE_BLACK_EVENT);
		
		this.wantsToMoveBlack = wantsToMoveBlack;
	}

	public boolean isWantsToMoveBlack() {
		return wantsToMoveBlack;
	}
}
