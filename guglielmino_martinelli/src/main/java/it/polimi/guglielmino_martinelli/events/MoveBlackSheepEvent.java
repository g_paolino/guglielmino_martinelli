package it.polimi.guglielmino_martinelli.events;

import it.polimi.guglielmino_martinelli.Coordinates;

/**
 * Evento generato al momento dello spostamento della pecora nera.
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public class MoveBlackSheepEvent extends GenericEvent{

	private static final long serialVersionUID = 4061114355193490991L;
	private Coordinates xy;

	public MoveBlackSheepEvent(Coordinates xy){
		super(EventType.MOVE_BLACK_SHEEP_EVENT);
		this.xy = xy;
	}

	public Coordinates getXY(){
		return xy;
	}
}