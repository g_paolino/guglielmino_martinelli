package it.polimi.guglielmino_martinelli.events;

/**
 * Evento generato al click su una regione.
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public class ChosedRegionEvent extends GenericEvent{	

	private static final long serialVersionUID = -675793933755673610L;
	private int rgb;

	public ChosedRegionEvent(int rgb){
		super(EventType.CHOSED_REGION_EVENT);
		this.rgb = rgb;
	}

	public int getRgb(){
		return rgb;
	}
}