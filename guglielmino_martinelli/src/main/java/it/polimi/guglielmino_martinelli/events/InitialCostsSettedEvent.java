package it.polimi.guglielmino_martinelli.events;


/**
 * Evento usato per segnalare il settaggio dei costi iniziali a 0 nel model.
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public class InitialCostsSettedEvent extends GenericEvent {

	private static final long serialVersionUID = 4939682879869168795L;

	public InitialCostsSettedEvent() {
		super(EventType.INITIAL_COSTS_SETTED_EVENT);
	}
}