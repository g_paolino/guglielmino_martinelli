package it.polimi.guglielmino_martinelli.events;

/**
 * Evento di cambio turno.
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public class ChangeTurnEvent extends GenericEvent{

	private static final long serialVersionUID = 4892833100692872940L;

	private String action;

	public ChangeTurnEvent(String action){
		super(EventType.CHANGE_TURN_EVENT);
		this.action = action;
	}

	public String getAction() {
		return action;
	}
}