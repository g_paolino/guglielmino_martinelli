package it.polimi.guglielmino_martinelli.events;

import it.polimi.guglielmino_martinelli.Coordinates;

/**
 * Evento generato al momento dello spostamento di una pecora bianca.
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public class MoveWhiteSheepEvent extends GenericEvent{

	private static final long serialVersionUID = -5875129465011438289L;
	private Coordinates source;
	private Coordinates destination;

	public MoveWhiteSheepEvent(Coordinates xy){
		super(EventType.MOVE_WHITE_SHEEP_EVENT);
		this.destination = xy;
	}

	public Coordinates getDestination(){
		return destination;
	}

	public Coordinates getSource() {
		return source;
	}

	public void setSource(Coordinates source) {
		this.source = source;
	}
}