package it.polimi.guglielmino_martinelli.events;

/**
 * Evento usato per segnalare l'inizio della fase iniziale di gioco.
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public class InitialPhaseEvent extends GenericEvent {

	private static final long serialVersionUID = 3841843944451213800L;

	public InitialPhaseEvent() {
		super(EventType.INITIAL_PHASE_EVENT);
	}
}