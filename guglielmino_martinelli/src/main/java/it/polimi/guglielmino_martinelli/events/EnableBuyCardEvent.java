package it.polimi.guglielmino_martinelli.events;

public class EnableBuyCardEvent extends GenericEvent{
	
	private static final long serialVersionUID = -2022597275952309331L;
	private boolean enable;
	
	public EnableBuyCardEvent(boolean enable) {
		super(EventType.ENABLE_BUY_CARD_EVENT);
		this.enable = enable;
	}

	public boolean isEnable() {
		return enable;
	}
}
