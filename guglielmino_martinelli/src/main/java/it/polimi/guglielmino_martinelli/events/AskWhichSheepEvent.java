package it.polimi.guglielmino_martinelli.events;

public class AskWhichSheepEvent extends GenericEvent{

	private static final long serialVersionUID = -1864102882062382777L;

	public AskWhichSheepEvent() {
		super(EventType.ASK_WHICH_SHEEP_EVENT);
	}

}
