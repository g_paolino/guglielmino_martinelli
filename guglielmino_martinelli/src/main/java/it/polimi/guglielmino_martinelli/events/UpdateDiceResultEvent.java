package it.polimi.guglielmino_martinelli.events;


/**
 * Evento di modifica del valore assunto dal dado dopo il lancio.
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public class UpdateDiceResultEvent extends GenericEvent {

	private static final long serialVersionUID = -7113081939641473942L;
	/**
	 * Nuovo valore del dado.
	 */
	private int diceResult;

	public UpdateDiceResultEvent(int diceResult) {
		super(EventType.UPDATE_DICE_RESULT_EVENT);
		this.diceResult = diceResult;
	}

	public int getDiceResult(){
		return diceResult;
	}
}