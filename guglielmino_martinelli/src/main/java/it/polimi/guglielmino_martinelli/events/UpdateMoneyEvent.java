package it.polimi.guglielmino_martinelli.events;

/**
 * Evento generato al momento dell'aggiornamento dei danari di un giocatore nel model.
 * @author paologuglielmino
 *
 */
public class UpdateMoneyEvent extends GenericEvent {

	private static final long serialVersionUID = 1197874186522659745L;

	private int money;

	public UpdateMoneyEvent (int money){
		super(EventType.UPDATE_MONEY_EVENT);
		this.money=money;
	}
	public int getMoney(){
		return money;
	}
}