package it.polimi.guglielmino_martinelli.events;


/**
 * Evento generato quando i nomi dei giocatori vengono settati nel model (gioco locale).
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public class PlayersNamesSettedEvent extends GenericEvent {

	private static final long serialVersionUID = 8663418979485430782L;

	public PlayersNamesSettedEvent() {
		super(EventType.PLAYERS_NAMES_SETTED_EVENT);
	}
}