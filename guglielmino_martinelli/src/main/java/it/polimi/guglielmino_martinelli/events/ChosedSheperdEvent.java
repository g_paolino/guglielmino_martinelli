package it.polimi.guglielmino_martinelli.events;

/**
 * Evento che indica la scelta, per il turno corrente, di uno dei pastori del giocatore (caso 2 giocatori).
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public class ChosedSheperdEvent extends GenericEvent{

	private static final long serialVersionUID = -3238088235612318860L;
	/**
	 * Strada in cui è posizionato il pastore scelto.
	 */
	private int sheperdIndex;

	public ChosedSheperdEvent(int sheperdIndex){
		super(EventType.CHOSED_SHEPERD_EVENT);
		this.sheperdIndex = sheperdIndex;
	}

	public int getSheperdIndex(){
		return sheperdIndex;
	}
}