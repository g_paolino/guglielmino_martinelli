package it.polimi.guglielmino_martinelli.events;

import it.polimi.guglielmino_martinelli.Coordinates;

/**
 * Evento generato al momento della scelta della pecora da muovere.
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public class ChosedSheepToMoveEvent extends GenericEvent{

	private static final long serialVersionUID = 5011725749534599964L;
	private Coordinates xy;

	public ChosedSheepToMoveEvent(Coordinates xy){
		super(EventType.CHOSED_SHEEP_TO_MOVE_EVENT);
		this.xy = xy;
	}

	public Coordinates getXY(){
		return xy;
	}
}