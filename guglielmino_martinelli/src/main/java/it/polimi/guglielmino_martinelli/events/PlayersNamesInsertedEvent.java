package it.polimi.guglielmino_martinelli.events;

import java.util.List;

/**
 * Evento generato quando i giocatori inseriscono i loro nomi (gioco locale).
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public class PlayersNamesInsertedEvent extends GenericEvent {

	private static final long serialVersionUID = -7974057229162127335L;

	private List<String> names;

	public PlayersNamesInsertedEvent(List<String> names) {
		super(EventType.PLAYERS_NAMES_INSERTED_EVENT);
		this.names = names;

	}

	public List<String> getNames() {
		return names;
	}
}