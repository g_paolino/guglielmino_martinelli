package it.polimi.guglielmino_martinelli.events;

public class PlayersReadyEvent extends GenericEvent{

	private static final long serialVersionUID = -3252654426146004246L;
	
	public PlayersReadyEvent() {
		super(EventType.PLAYERS_READY_EVENT);
	}
}