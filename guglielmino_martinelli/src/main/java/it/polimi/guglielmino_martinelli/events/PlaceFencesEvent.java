package it.polimi.guglielmino_martinelli.events;

import it.polimi.guglielmino_martinelli.Coordinates;

/**
 * Evento generato al momento del posizionamento di un recinto (occupazione di una strada).
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public class PlaceFencesEvent extends GenericEvent{

	private static final long serialVersionUID = -2009707417287097090L;

	private Coordinates xy;
	
	private boolean finalFence;

	public PlaceFencesEvent(Coordinates xy, boolean finalFence){
		super(EventType.PLACE_FENCES_EVENT);
		this.xy = xy;
		this.finalFence = finalFence;
	}

	public Coordinates getXY(){
		return xy;
	}

	public boolean isFinalFence() {
		return finalFence;
	}
}