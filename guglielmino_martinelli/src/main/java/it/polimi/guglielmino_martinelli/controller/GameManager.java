package it.polimi.guglielmino_martinelli.controller;

import it.polimi.guglielmino_martinelli.events.ChosedRegionEvent;
import it.polimi.guglielmino_martinelli.events.ChosedRoadEvent;
import it.polimi.guglielmino_martinelli.events.ChosedSheperdEvent;
import it.polimi.guglielmino_martinelli.events.GenericEvent;
import it.polimi.guglielmino_martinelli.events.PlayerNameInsertedEvent;
import it.polimi.guglielmino_martinelli.events.PlayersNamesInsertedEvent;
import it.polimi.guglielmino_martinelli.events.WantsToMoveBlackEvent;
import it.polimi.guglielmino_martinelli.model.ChosedCardEvent;
import it.polimi.guglielmino_martinelli.model.GameStatus;
import it.polimi.guglielmino_martinelli.model.GraphNode;
import it.polimi.guglielmino_martinelli.model.LandType;
import it.polimi.guglielmino_martinelli.model.Player;
import it.polimi.guglielmino_martinelli.model.Region;
import it.polimi.guglielmino_martinelli.model.Road;
import it.polimi.guglielmino_martinelli.view.View;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Observable;
import java.util.Observer;
import java.util.Random;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Classe che si occupa di gestire la logica del gioco.
 * 
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public class GameManager implements Observer{

	/**
	 * Oggetto che contine l'intero stato della partita.
	 */
	private GameStatus status;
	/**
	 * Oggetto che contiene la view di gioco, non utilizzato nell'applicazione
	 * perchè tutta la grafica viene aggiornata a seguito di modifiche
	 * al model. Tuttavia in caso di modifiche  al software è possibile usare
	 * questa istanza di GameView per decidere le schermate visualizzate
	 * all'utente.
	 */
	@SuppressWarnings("unused")
	private View gameView;
	/**
	 * Variabile che indica l'azione dichiarata.
	 */
	private String action;

	public GameManager(GameStatus status, View gameView) {
		this.status = status;
		this.gameView = gameView;
		this.action = "PLACING_SHEPERD";
	}

	/**
	 * Avviamento fase iniziale di gioco.
	 */
	public void initialPhase() {

		// Carico la mappa (grafo) dal file XML in memoria.
		status.getGameMap().createGraph();

		// Posiziono le teseere terreno con i loro costi iniziali.
		status.setInitialCardsCosts();

		// Consegna delle tessere iniziali ai giocatori.
		deliverStartCardsToPlayers();

		// Random per decidere il primo giocatore
		status.setStarterPlayerIndex(randomStater());
		status.setCurrentPlayerIndex(status.getStarterPlayerIndex());

		// posizionamento iniziale del/dei pastore/i del primo giocatore.
		placeSheperds();
	}

	/**
	 * Inserisce un nuovo Player nel model dell'applicazione.
	 * 
	 * @param index indice del giocatore.
	 * @param name nome del giocatore.
	 */
	private void insertPlayer(int index, String name) {
		Player player = new Player(name, status.moneyPerPlayer(), index);
		// Dico che status osserva l'istanza di Player.
		player.addObserver(status);
		// Passo la modifica al model.
		status.insertPlayer(index, player);
	}

	/**
	 * Inserisce i giocatori, modalità di gioco locale.
	 * @param arg evento di inserimento nomi dei giocatori.
	 */
	private void insertPlayers(PlayersNamesInsertedEvent arg) {
		status.insertPlayers(arg);
	}

	/**
	 * Procedura per la distribuzione delle tessere terreno iniziali ai giocatori.
	 */
	private void deliverStartCardsToPlayers() {
		/* Creo una copia dei costi delle tessere iniziali, la uso per tenere
		 * conto delle tessere iniziali che ho distribuito ai giocatori.
		 */
		Map<LandType, Integer> startCardsUsed = new HashMap<LandType, Integer>(status.getCardsCurrentCost());
		LandType tile;

		// Procedimento che viene ripetuto per ciascun giocatore.
		for (int playerIndex = 1; playerIndex <= status.getPlayers().size(); playerIndex++) {
			do {
				// Faccio una scelta random di una delle tessere terreno iniziali.
				tile = LandType.randomChoiche();
			} while (startCardsUsed.get(tile) != 0);
			/*
			 * La scelta random è mutualmente esclusiva, 2 giocatori non possono
			 * avere la stessa tessera terreno iniziale, per cui tengo conto di
			 * quelle gia assegnate agli altri in questa fase nella Map
			 * startCardUsed.
			 */

			// Assegno al giocatore la tessera inziale scelta casualmente.
			status.assignCardToPlayer(tile, playerIndex);
			// Aggiorno la Map che tiene conto delle tessere iniziali già distribuite.
			startCardsUsed.put(tile, 1);
		}
	}

	/**
	 * Metdo che sceglie casualmente il giocatore che dovrà cominciare la partita.
	 * 
	 * @return indice del giocatore che dovrà cominciare.
	 */
	private int randomStater() {
		Random rnd = new Random();
		return 1 + rnd.nextInt(status.getNumberOfPlayers());
	}

	/**
	 * Metdo per l'inizio della fase di posizionamento delle pedine pastore.
	 */
	private void placeSheperds(){

		// Se il primo pastore del giocatore scelto non ha posizione.
		if (status.getPlayers().get(status.getCurrentPlayerIndex()).getPosition().get(1) == null) {
			status.setCurrentSheperdIndex(1);
			status.updateInstruction("Scegli una posizione iniziale", status.getCurrentPlayerIndex());
		}else{
			// Se si gioca in due ripete la scelta posizione anche per i secondi pastori.
			// Se il secondo pastore del giocatore scelto non ha posizione.
			if (status.getNumberOfPlayers() == 2 && status.getPlayers().get(status.getCurrentPlayerIndex()).getPosition().get(2) == null) {
				status.setCurrentSheperdIndex(2);
				status.updateInstruction("Scegli la seconda posizione", status.getCurrentPlayerIndex());
			}else{
				// Completati tutti i posizionamenti avvia il gioco.
				action = "NULL";
				playing();
			}
		}
	}

	/**
	 * Fase di gioco.
	 */
	private void playing() {
		action="NULL";
		if(!(status.isFinalPhase()) || status.getCurrentPlayerIndex() != status.getStarterPlayerIndex() || status.getActionsLeft() != 3){
			//Se siamo all'inizio del turno di gioco.
			if(status.getActionsLeft()==3 && !(status.isSheperdSelected())){
				//Lieve ritardo sulla fuga della pecora nera, per consentire al giocatore di vedere l animazioni alla fine del turno.
				Timer timer = new Timer();
				timer.schedule(new TimerTask(){
					@Override
					public void run() {
						blackSheepEscapes();
					}
				}, 1500);
			}
			//Se la partita è a 2 giocatori, bisogna scegliere quale pastore usare in questo turno.
			if(status.getNumberOfPlayers() == 2 && !(status.isSheperdSelected())){
				status.updateInstruction("Scegli un pastore", status.getCurrentPlayerIndex());
			}else{
				//Se la partita è a 3 o 4 giocatori il pastore è unico, quindi è selezionato in automatico.
				status.setSheperdSelected(true);
				status.updateInstruction("Scegli una mossa", status.getCurrentPlayerIndex());
			}
		}else{
			status.updateInstruction("La partita è finita", status.getCurrentPlayerIndex());
			Timer timer2 = new Timer();
			timer2.schedule(new TimerTask(){
				@Override
				public void run() {
					addProfit();
				}
			}, 2000);
		}
	}

	/**
	 * Esegue la fuga della pecora nera se si verificano le condizioni.
	 */
	private void blackSheepEscapes(){

		//Simulo il lancio di un dado a 6 facce.
		status.setDiceResult(throwDice());

		//Cerco la regione di partenza dell'azione, sarà la regione occupata dalla pecora nera.
		Region sourceRegion = status.getGameMap().findBlackSheepRegion();
		//Definisco un insieme delle possibili vie di fuga (strade adiacenti alla regione occupata dalla pecora nera).
		Set<Road> escapeRoads = status.getGameMap().getAdjacentRoads(sourceRegion);

		//Scorro le vie di fuga.
		for(Road x : escapeRoads){

			/*Controllo che mi permette di trovare la via di fuga indicata dal risultato del lancio del dado,
			inoltre verifico che questa via di fuga sia transitabile.*/
			if((x.getNumber() == status.getDiceResult()) && (!(x.isBusy()))){

				//Determino la destinazione, distinguendo tra regione di partenza e regione di arrivo.
				Region destinationRegion = status.getGameMap().getCorrectDestination(x, sourceRegion);
				//Nella regione di partenza registro l'assenza della pecora nera.
				sourceRegion.setBlackSheep(false);
				//Nella regione di destinazione registro l'arrivo della pecora nera.
				destinationRegion.setBlackSheep(true);
			}
		}
	}

	/**
	 * Funzione che simula il lancio di un dado.
	 * @return numero intero compreso tra 1 e 6 estremi inclusi.
	 */
	private int throwDice(){
		Random generator = new Random();
		return 1 + generator.nextInt(6);
	}

	/**
	 * Procedura di spostamento del pastore selezionato.
	 * @param destination strada di destinazione del pastore.
	 */
	private void moveSheperd(Road destination){
		int toll = 1;

		//Se la destinazione è adiacente al pastore non c'è alcun pedaggio da pagare.
		if(isNodeNearSheperd(destination)){
			toll = 0;
		}

		//Il pastore paga il suo pedaggio.
		status.getPlayers().get(status.getCurrentPlayerIndex()).setMoney(status.getPlayers().get(status.getCurrentPlayerIndex()).getMoney() - toll);

		//Il pastore piazza un recinto.
		status.setFencesAvailable(status.getFencesAvailable()-1, status.getPlayers().get(status.getCurrentPlayerIndex()).getPosition().get(status.getCurrentSheperdIndex()));

		//Aggiorno la posizione del pastore.
		status.getPlayers().get(status.getCurrentPlayerIndex()).getPosition().put(status.getCurrentSheperdIndex(), destination.getXY());

		//Aggiorno lo stato del turno corrente.
		sheperdMoved();
	}

	/**
	 * Funzione che data in ingresso un nodo (strada o regione) dice se il nodo è adiacente al pastore (true) oppure no (false).
	 * @param node nodo di cui si vuole verificare l'adiacenza.
	 * @return valore di verità true o false.
	 */
	private boolean isNodeNearSheperd(GraphNode node){

		Road oldRoad = null;

		//Ricavo la strada su cui è posizionato il pastore corrente.
		for(Road x : status.getGameMap().getAllRoads().values()){
			if((status.getPlayers().get(status.getCurrentPlayerIndex()).getPosition().get(status.getCurrentSheperdIndex())).equals(x.getXY())){
				oldRoad = x;
			}
		}

		//Controllo l'esistenza del collegamento tra strada dove si trova il pastore e nodo (strada o regione) di cui voglio verificare l'adiacenza.
		if(status.getGameMap().getGraph().containsEdge(oldRoad, node)){
			//Il nodo è adicente al pastore.
			return true;
		}
		//Il nodo non è adiacente al pastore.
		return false;
	}

	/**
	 * Azioni da effettuare in caso di spostamento del pastore.
	 */
	private void sheperdMoved(){
		action="NULL";
		//Decremento di 1 il numero di mosse in questo turno rimaste al giocatore.
		status.setActionsLeft(status.getActionsLeft()-1);
		//Autorizzo il giocatore a muovere pecore.
		status.setCanMoveSheep(true);
		//Autorizzo il giocatore a comprare tessere terreno.
		status.setCanBuyCard(true);
	}

	/**
	 * Procedura per lo spostamento di una pecora da una regione di partenza all destinazione prossima.
	 * @param sourceRegion regione di partenza.
	 */
	private void moveSheep(Region sourceRegion){

		Road road = null;

		//Ricavo la strada su cui è posizionato il pastore.
		for(Road r : status.getGameMap().getAllRoads().values()){
			if(status.getPlayers().get(status.getCurrentPlayerIndex()).getPosition().get(status.getCurrentSheperdIndex()).equals(r.getXY())){
				road = r;
			}
		}

		//Identifico la regione di destinazione dove sposterò la pecora.
		Region destinationRegion = status.getGameMap().getCorrectDestination(road, sourceRegion);

		if(status.isWantsToMoveBlack()){
			//Aggiorno la posizione della pecora nera.
			sourceRegion.setBlackSheep(false);
			destinationRegion.setBlackSheep(true);
		}else{
			//Decremento di 1 le pecore bianche nella ragione di partenza.
			sourceRegion.setWhiteSheep(sourceRegion.getWhiteSheep()-1);
			//Aumento di 1 le pecore bianche nella regione di arrivo.
			destinationRegion.setWhiteSheep(destinationRegion.getWhiteSheep()+1);	
		}
		//Aggiorno lo stato del turno corrente.
		sheepMoved();

		return;
	}

	/**
	 * Azioni da effettuare in caso di spostamento pecora.
	 */
	private void sheepMoved(){
		action="NULL";
		//Proibisco al giocatore di muovere ancora pecore bianche.
		status.setCanMoveSheep(false);
		//Decremento di 1 il numero di mosse in questo turno rimaste al giocatore.
		status.setActionsLeft(status.getActionsLeft()-1);
	}

	private boolean areThereAnySheepNear(){
		for(Road roa : status.getGameMap().getAllRoads().values()){
			if(roa.getXY().equals(status.getPlayers().get(status.getCurrentPlayerIndex()).getPosition().get(status.getCurrentSheperdIndex()))){
				for(Region reg : status.getGameMap().getAdjacentRegions(roa)){
					if(reg.getBlackSheep() || reg.getWhiteSheep()>0){
						return true;
					}
				}
			} 
		}

		return false;
	}
	
	private boolean areThereAnyGameCardToBuy(){
		for(Road roa : status.getGameMap().getAllRoads().values()){
			if(roa.getXY().equals(status.getPlayers().get(status.getCurrentPlayerIndex()).getPosition().get(status.getCurrentSheperdIndex()))){
				for(Region reg : status.getGameMap().getAdjacentRegions(roa)){
					if(!(LandType.SHEEPSBURG.equals(reg.getType()))){
						if(status.getCardsCurrentCost().get(reg.getType())<5){
							return true;
						}
					}
				}
			} 
		}

		return false;
	}
	
	

	/**
	 * Dato una valore rgb preso dalla mappa di gioco restituisce la regione corrispondente.
	 * @param rgb valore del colore preso dalla mappa di gioco.
	 * @return regione con il valore rgb fornito.
	 */
	private Region fromRgbToRegion(int rgb){
		for(Region r : status.getGameMap().getAllRegions().values()){
			if(rgb == r.getRGB()){
				return r;
			}				
		}
		return null;
	}

	private boolean isRegionNearToSheperd(Region region){
		for(Region reg : status.getGameMap().getAllRegions().values()){
			if(region.getXY().equals(reg.getXY())){
				for(Road roa : status.getGameMap().getAdjacentRoads(reg)){
					if(roa.getXY().equals(status.getPlayers().get(status.getCurrentPlayerIndex()).getPosition().get(status.getCurrentSheperdIndex()))){
						return true;
					} 
				}
			}
		}
		return false;
	}

	private boolean isLandNearToSheperd(LandType land){
		for(Road roa : status.getGameMap().getAllRoads().values()){
			if(roa.getXY().equals(status.getPlayers().get(status.getCurrentPlayerIndex()).getPosition().get(status.getCurrentSheperdIndex()))){
				for(Region reg : status.getGameMap().getAdjacentRegions(roa)){
					if(land.equals(reg.getType())){
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * Procedura di acquisto di una tessera terreno del tipo adiacente al pastore selezionato.
	 * @param region regione del tipo di terreno che voglio acquistare.
	 */
	private void buyCard(LandType tile){

		int tileCost = status.getCardsCurrentCost().get(tile);

		//Verifico se effettivamente il pastore può acquistare una tessera di quel tipo (ovvero se si trova vicino ad una regione di quel tipo).
		if(status.getPlayers().get(status.getCurrentPlayerIndex()).getMoney()>=tileCost && tileCost<5){

			//Il giocatore paga il costo della tessera terreno.
			status.getPlayers().get(status.getCurrentPlayerIndex()).setMoney(status.getPlayers().get(status.getCurrentPlayerIndex()).getMoney() - tileCost);

			//Aggiorno il numero di tessere di quel tipo possedute dal pastore.
			status.assignCardToPlayer(tile, status.getCurrentPlayerIndex());

			//Aggiorno il costo delle tessere terreno di quel tipo.
			status.updateCardsCurrentCost(tile);

			//Aggiorno lo stato del turno corrente.
			cardBuyed();

			return;
		}
	}

	/**
	 * Azioni da effettuare in caso di acquisto di una tessera terreno.
	 */
	private void cardBuyed(){
		action="NULL";
		//Proibisco al giocatore di comprare ancora tessere terreno.
		status.setCanBuyCard(false);
		//Decremento di 1 il numero di mosse in questo turno rimaste al giocatore.
		status.setActionsLeft(status.getActionsLeft()-1);
	}

	/**
	 * Procedura che aggiunge ai giocatori i guadagni derivanti dai tipi di
	 * terreno posseduti.
	 */
	private void addProfit() {

		// Ricavo l'associazione tipo di terreno e numero di pecore in quel
		// terreno.
		Map<LandType, Integer> numberOfSheep = status.getGameMap().sheepPerLandType();

		// Aggiungo 2 pecore (valore della pecora nera) al tipo di terreno che
		// ha la pecora nera.
		LandType blackSheepIsHere = status.getGameMap().findBlackSheepRegion().getType();
		numberOfSheep.put(blackSheepIsHere,numberOfSheep.get(blackSheepIsHere) + 2);

		// Scorro tutti i giocatori.
		for (Player p : status.getPlayers().values()) {

			// Contatore del guadagno del giocatore dovuto alle tessere terreno
			// possedute.
			int profit = 0;

			// Scorro tutte le tessere terreno possedute dal giocatore.
			for (Entry<LandType, Integer> entry : p.getCardsOwned().entrySet()) {

				LandType land = entry.getKey();
				// Numero di tessere di un certo tipo possedute dal giocatore.
				int numberOfCardsOfLand = entry.getValue();

				// Numero di pecore in un certo tipo di terreno.
				int numberOfSheepInLand = numberOfSheep.get(land);

				// Aggiornamento dei guadagni del giocatore dovuti a quel
				// terreno.
				profit += numberOfCardsOfLand * numberOfSheepInLand;
			}

			// Aggiornamento dei soldi totali del giocatore.
			p.setMoney(p.getMoney() + profit);
		}

		status.showFinalScreen(status.getPlayers());

	}

	@Override
	public void update(Observable o, Object arg) {

		switch(((GenericEvent)arg).getType()){

		//Sono stati inseriti i nomi dei giocatori.
		case PLAYERS_NAMES_INSERTED_EVENT:
			insertPlayers((PlayersNamesInsertedEvent)arg);
			break;

			//La fase iniziale ha inizio.
		case INITIAL_PHASE_EVENT:
			initialPhase();
			break;

			//E' stato inserito il nome di un giocatore.
		case PLAYER_NAME_INSERTED_EVENT:
			insertPlayer(((PlayerNameInsertedEvent)arg).getPlayerIndex(), ((PlayerNameInsertedEvent)arg).getName());
			break;

			//E' stata cliccata una strada.
		case CHOSED_ROAD_EVENT:
			if(((ChosedRoadEvent)arg).getPlayerIndex() == status.getCurrentPlayerIndex() || ((ChosedRoadEvent)arg).getPlayerIndex() == 0){
				for(Road r : status.getGameMap().getAllRoads().values()){
					if(((ChosedRoadEvent)arg).getXY().equals(r.getXY())){

						if("PLACING_SHEPERD".equals(action)){
							r.setBusy(true, status.getCurrentSheperdIndex());
							status.getPlayers().get(status.getCurrentPlayerIndex()).getPosition().put(status.getCurrentSheperdIndex(), r.getXY());
							status.setActionsLeft(0);
							placeSheperds();
						}
						if("MOVING_SHEPERD".equals(action)){
							r.setBusy(true, status.getCurrentSheperdIndex());
							moveSheperd(r);
							playing();
						}
					}
				}
			}
			break;

			//E' stato scelto uno dei due pastori del giocatore cliccando su di esso.
		case CHOSED_SHEPERD_EVENT:
			if(((ChosedSheperdEvent)arg).getPlayerIndex() == status.getCurrentPlayerIndex() && "NULL".equals(action) && !(status.isSheperdSelected())){
				status.setSheperdSelected(true);
				status.setCurrentSheperdIndex(((ChosedSheperdEvent)arg).getSheperdIndex());
				playing();
			}
			break;

			//E' stata scelta muovi pastore come azione da compiere.
		case CHOSED_MOVE_SHEPERD_EVENT:
			if("NULL".equals(action) && status.isSheperdSelected()){
				status.updateInstruction("Scegli una destinazione", status.getCurrentPlayerIndex());
				action="MOVING_SHEPERD";
			}
			break;

			//E' stata scelta muovi pecora come azione da compiere.
		case CHOSED_MOVE_SHEEP_EVENT:
			if("NULL".equals(action) && status.isSheperdSelected()){
				if(areThereAnySheepNear()){
					status.updateInstruction("Scegli da dove", status.getCurrentPlayerIndex());
					action="MOVING_SHEEP";
				}else{
					status.updateInstruction("<HTML>Non ci sono pecore vicine<br/><center>scegli un'altra mossa</center></HTML>", status.getCurrentPlayerIndex());
				}
			}									 
			break;

			//E' stata cliccata una regione.
		case CHOSED_REGION_EVENT:
			if(((ChosedRegionEvent)arg).getRgb()!=0){
				if("MOVING_SHEEP".equals(action) && status.isSheperdSelected() && isRegionNearToSheperd(fromRgbToRegion(((ChosedRegionEvent)arg).getRgb()))){
					if(fromRgbToRegion(((ChosedRegionEvent)arg).getRgb()).getWhiteSheep()>0 && fromRgbToRegion(((ChosedRegionEvent)arg).getRgb()).getBlackSheep()){
						status.setRegionClicked(fromRgbToRegion(((ChosedRegionEvent)arg).getRgb()));
						status.askWhichSheep();
					}else{
						if(fromRgbToRegion(((ChosedRegionEvent)arg).getRgb()).getWhiteSheep()>0){
							status.setWantsToMoveBlack(false);
							status.setRegionClicked(fromRgbToRegion(((ChosedRegionEvent)arg).getRgb()));
							moveSheep(fromRgbToRegion(((ChosedRegionEvent)arg).getRgb()));
							playing();	
						}else{
							if(fromRgbToRegion(((ChosedRegionEvent)arg).getRgb()).getBlackSheep()){
								status.setWantsToMoveBlack(true);
								moveSheep(fromRgbToRegion(((ChosedRegionEvent)arg).getRgb()));
								playing();
							}else{
								status.updateInstruction("Questa regione non ha pecore", status.getCurrentPlayerIndex());
							}
						}
					}
				}
			}
			break;

			//L'utente ha deciso di muovere la pecora nera.
		case WANTS_TO_MOVE_BLACK_EVENT:
			status.setWantsToMoveBlack(((WantsToMoveBlackEvent)arg).isWantsToMoveBlack());
			moveSheep(status.getRegionClicked());
			playing();
			break;

			//Il giocatore ha premuto il bottone compra tessera terreno.
		case CHOSED_BUY_CARD_EVENT:
			if("NULL".equals(action) && status.isSheperdSelected()){
				if(areThereAnyGameCardToBuy()){
					status.updateInstruction("Scegli quale comprare", status.getCurrentPlayerIndex());
					action="BUYING_CARD";
				}else{
					status.updateInstruction("<HTML>Tessere terreno vicine esaurite<br/><center>scegli un'altra mossa</center></HTML>", status.getCurrentPlayerIndex());
				}
			}
			break;

			//Il giocatore ha cliccato la tessera che intende comprare.
		case CHOSED_CARD_EVENT:
			if("BUYING_CARD".equals(action) && status.isSheperdSelected() && isLandNearToSheperd(((ChosedCardEvent)arg).getLandType())){
				buyCard(((ChosedCardEvent)arg).getLandType());
				playing();	
			}
			break;

		default:
			break;
		}
	}
}