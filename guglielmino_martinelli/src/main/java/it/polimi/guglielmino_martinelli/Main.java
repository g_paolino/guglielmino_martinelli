package it.polimi.guglielmino_martinelli;

import it.polimi.guglielmino_martinelli.view.ModeView;

import java.util.logging.Logger;

/**
 * Main del programma.
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public class Main {
	
	public static final Logger LOGGER = Logger.getLogger("it.polimi.guglielmino_martinelli");
	
	//Costruttore private per nascondere quello pubblico di default (Suggerimento di SonarQube).
	private Main(){
	}
	
	public static void main(String[] args){
		//Lancio la schermata di scelta della modalità di gioco.
		new Thread(new ModeView()).start();
	}
}