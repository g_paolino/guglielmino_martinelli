package it.polimi.guglielmino_martinelli.model;

import it.polimi.guglielmino_martinelli.Coordinates;
import it.polimi.guglielmino_martinelli.events.UpdateMoneyEvent;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;

/**
 * Questa classe rappresenta un giocatore.
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public class Player extends Observable implements Serializable{

	private static final long serialVersionUID = 6466836056762649859L;
	/**
	 * Rappresenta il nome del giocatore.
	 */
	private String name;
	/**
	 * Rappresenta l'ammontare di danari del il giocatore.
	 */
	private int money;
	/**
	 * Indice del giocatore.
	 */
	private int index;
	/**
	 * Rappresenta l'insieme di tessete terreno che possiede il giocatore.
	 */
	private Map<LandType, Integer> cardsOwned;
	/**
	 * Indica per ogni giocatore la posizione del pastore (caso 3-4 giocatori) oppure le posizioni dei due pastori (caso 2 giocatori) all'interno dela mappa.
	 */
	private Map<Integer, Coordinates> position;

	public Player(String name, int money, int index){

		this.name = name;
		this.money = money;
		this.index = index;
		this.cardsOwned = setupCardsOwned();
		this.position = new HashMap<Integer, Coordinates>();
	}

	/**
	 * Imposta a 0 le tessere terreno possedute dal giocatore inizialmente.
	 * @return HashMap con corrispondenze tra tipo terreno e quantità posseduta.
	 */
	private Map<LandType, Integer> setupCardsOwned(){
		Map<LandType, Integer> defaultCardsOwned = new HashMap<LandType, Integer>();
		for(LandType x : LandType.values()){
			if(!(x.equals(LandType.SHEEPSBURG))){
				defaultCardsOwned.put(x, 0);
			}
		}
		return defaultCardsOwned;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public int getMoney() {
		return money;
	}

	public void setMoney(int money) {
		this.money = money;

		UpdateMoneyEvent event = new UpdateMoneyEvent(money);
		event.setPlayerIndex(index);
		setChanged();
		notifyObservers(event);
	}

	public Map<LandType, Integer> getCardsOwned() {
		return cardsOwned;
	}

	public void setPosition(int sheperdIndex, Coordinates xy) {
		position.put(sheperdIndex, xy);
	}

	public Map<Integer, Coordinates> getPosition() {
		return position;
	}

	public int getIndex() {
		return index;
	}

	@Override
	public String toString() {
		return "Player [name=" + name + ", money=" + money + ", cardsOwned="
				+ cardsOwned + ", position=" + position + "]";
	}
}