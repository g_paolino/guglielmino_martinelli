package it.polimi.guglielmino_martinelli.model;

import it.polimi.guglielmino_martinelli.events.GenericEvent;

/**
 * Evento di aggiunta di una determinata strada al grafo.
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public class AddRoadEvent extends GenericEvent {

	private static final long serialVersionUID = -6166813543871997058L;
	/**
	 * Strada aggiunta.
	 */
	private Road road;

	public AddRoadEvent(Road road){
		super(EventType.ADD_ROAD_EVENT);
		this.road = road;
	}

	public Road getRoad() {
		return road;
	}
}