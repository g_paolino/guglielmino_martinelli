package it.polimi.guglielmino_martinelli.model;

import it.polimi.guglielmino_martinelli.events.GenericEvent;

import java.util.Map;

public class UpdateCardsCurrentCostEvent extends GenericEvent{

	private static final long serialVersionUID = 7386895288324189196L;
	private Map<LandType, Integer> cardsCurrentCost;
	
	public UpdateCardsCurrentCostEvent(Map<LandType, Integer> cardsCurrentCost) {
		super(EventType.UPDATE_CARDS_CURRENT_COST_EVENT);
		this.cardsCurrentCost = cardsCurrentCost;
	}
	
	public Map<LandType, Integer> getCardsCurrentCost(){
		return cardsCurrentCost;
	}

}
