package it.polimi.guglielmino_martinelli.model;

import it.polimi.guglielmino_martinelli.events.GenericEvent;

public class ChosedCardEvent extends GenericEvent{

	private static final long serialVersionUID = -3012326261025195510L;
	private LandType landType;
	
	public ChosedCardEvent(LandType landType) {
		super(EventType.CHOSED_CARD_EVENT);
		this.landType = landType;
	}

	public  LandType getLandType() {
		return landType;
	}

}
