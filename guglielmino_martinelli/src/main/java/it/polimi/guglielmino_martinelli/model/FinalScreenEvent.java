package it.polimi.guglielmino_martinelli.model;

import it.polimi.guglielmino_martinelli.events.GenericEvent;

import java.util.Map;

public class FinalScreenEvent extends GenericEvent{
	
	private static final long serialVersionUID = -3904277647455409440L;
	private Map<Integer, Player> players;

	public FinalScreenEvent(Map<Integer, Player> players) {
		super(EventType.FINAL_SCREEN_EVENT);
		this.players = players;
	}

	public Map<Integer, Player> getPlayers() {
		return players;
	}	

}
