package it.polimi.guglielmino_martinelli.model;

import it.polimi.guglielmino_martinelli.events.GenericEvent;

/**
 * Evento relativo al cambio del giocatore corrente.
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public class PlayerChangedEvent extends GenericEvent{

	private static final long serialVersionUID = -473618122984884172L;
	/**
	 * Indice del giocatore corrente.
	 */
	private Player player;

	public PlayerChangedEvent(Player player){
		super(EventType.PLAYER_CHANGED_EVENT);
		this.player = player;
	}

	public Player getPlayer(){
		return player;
	}
}