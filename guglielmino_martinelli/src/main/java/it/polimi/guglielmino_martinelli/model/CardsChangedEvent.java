package it.polimi.guglielmino_martinelli.model;

import it.polimi.guglielmino_martinelli.events.GenericEvent;

import java.util.Map;

/**
 * Evento di modifica delle tessere terreno possedute.
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public class CardsChangedEvent extends GenericEvent {

	private static final long serialVersionUID = 7962257606585550709L;

	private Map<LandType,Integer> cardsOwned;

	public CardsChangedEvent(Map<LandType,Integer> cardsOwned) {
		super(EventType.CARDS_CHANGED_EVENT);
		this.cardsOwned = cardsOwned;
	}

	public Map<LandType, Integer> getCardsOwned() {
		return cardsOwned;
	}
}