package it.polimi.guglielmino_martinelli.model;

import it.polimi.guglielmino_martinelli.Coordinates;
import it.polimi.guglielmino_martinelli.events.MoveBlackSheepEvent;
import it.polimi.guglielmino_martinelli.events.MoveWhiteSheepEvent;

import java.io.Serializable;
import java.util.Observable;

/**
 * Questa classe rappresenta un oggetto regione.
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public class Region extends Observable implements GraphNode, Serializable {

	private static final long serialVersionUID = 5592617165944901789L;
	/**
	 * Tipo di terreno della regione.
	 */
	private LandType type;
	/**
	 * Indica la presenza o meno della pecora nera nella regione.
	 */
	private boolean blackSheep;
	/**
	 * Indica il numero di pecore bianche presenti nella regione.
	 */
	private int whiteSheep;
	/**
	 * Indica il colore della regione nella Game_Bored_Grey.
	 */
	private int rgb;
	/**
	 * Indica le coordinate x e y della strada nella mappa.
	 */
	private Coordinates xy;

	public void initSheepsburg(){
		this.blackSheep = true;
		this.whiteSheep = 0;
	}

	public void initRegion(){
		this.blackSheep = false;
		this.whiteSheep = 1;
	}

	public LandType getType() {
		return type;
	}

	public void setType(LandType type) {
		this.type = type;
	}

	public boolean getBlackSheep() {
		return blackSheep;
	}

	public void setBlackSheep(boolean blackSheep) {
		this.blackSheep = blackSheep;

		if(blackSheep){
			MoveBlackSheepEvent event = new MoveBlackSheepEvent (this.xy);
			setChanged();
			notifyObservers(event);
		}
	}

	public int getWhiteSheep() {
		return whiteSheep;
	}

	public void setWhiteSheep(int whiteSheep) {
		if(this.whiteSheep<whiteSheep){
			MoveWhiteSheepEvent event = new MoveWhiteSheepEvent(this.xy);
			setChanged();
			notifyObservers(event);
		}

		this.whiteSheep = whiteSheep;
	}

	public int getRGB() {
		return rgb;
	}

	public void setRGB(int rgb) {
		this.rgb = rgb;
	}

	public Coordinates getXY() {
		return xy;
	}

	public void setXY(Coordinates xy) {
		this.xy = xy;

		AddRegionEvent event = new AddRegionEvent(this);
		setChanged();
		notifyObservers(event);
	}

	@Override
	public String toString() {
		return "Region [type=" + type
				+ ", blackSheep="+ blackSheep
				+ ", whiteSheep=" + whiteSheep + "]";
	}
}