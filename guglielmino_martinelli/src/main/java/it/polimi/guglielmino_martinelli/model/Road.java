package it.polimi.guglielmino_martinelli.model;

import it.polimi.guglielmino_martinelli.Coordinates;
import it.polimi.guglielmino_martinelli.events.OccupiedRoadEvent;

import java.io.Serializable;
import java.util.Observable;

/**
 * Questa classe rappresenta un oggetto strada.
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public class Road extends Observable implements GraphNode, Serializable {

	private static final long serialVersionUID = 4683554154500244228L;
	/**
	 * Numero caratteristico della strada.
	 */
	private int number;
	/**
	 * Indica se la strada è occupata da un recinto o un pastore.
	 */
	private boolean busy;
	/**
	 * Indica le coordinate x e y della strada nella mappa.
	 */
	private Coordinates xy;

	/**
	 * Nel costruttore metto a false la varibile busy, inizialmente la strada non è occupata da nessuno.
	 */
	public Road() {
		super();
		this.busy = false;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public boolean isBusy() {
		return busy;
	}

	public void setBusy(boolean busy, int sheperdIndex) {
		this.busy = busy;

		OccupiedRoadEvent event = new OccupiedRoadEvent(this.xy, sheperdIndex);
		setChanged();
		notifyObservers(event);
	}

	public Coordinates getXY() {
		return xy;
	}

	public void setXY(Coordinates xy) {
		this.xy = xy;

		//Creo l'evento dell'aggiunta delle coordinate X e Y
		AddRoadEvent event = new AddRoadEvent(this);
		setChanged();
		notifyObservers(event);
	}

	@Override
	public String toString() {
		return "Road [number=" + number + ", busy=" + busy + ", Coordinates="+ xy +"]";
	}
}