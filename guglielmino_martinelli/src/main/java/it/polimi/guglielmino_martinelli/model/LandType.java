package it.polimi.guglielmino_martinelli.model;

import java.util.Random;

/**
 * Enumerazione dei possibili tipi di terreno.
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public enum LandType {

	FIELD,
	FOREST,
	HEATH,
	LAKE,
	MOUNTAIN,
	PLAIN,
	SHEEPSBURG;

	/**
	 * Funzione che restituisce un tipo di terreno casuale, ad eccezione di quello in ultima posizione (sheepsburg).
	 * @return LandType a scelta casuale tra tutti i possibili.
	 */
	public static LandType randomChoiche(){
		Random rnd = new Random();
		return values()[rnd.nextInt(LandType.values().length-1)];
	}

	/**
	 * Funzione che fa il parsing di una stringa in input e la converte in un elemento dell'enum.
	 * @param input stringa in ingresso.
	 * @return il valore corretto di tipo LandType corrispondente alla stringa in ingresso.
	 */
	public static LandType parseType(String input){
		return Enum.valueOf(LandType.class, input.toUpperCase());
	}
}