package it.polimi.guglielmino_martinelli.model;

/**
 * Interfaccia che definisce un nodo del grafo in modo astratto, sia esso regione o strada.
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public interface GraphNode {

}