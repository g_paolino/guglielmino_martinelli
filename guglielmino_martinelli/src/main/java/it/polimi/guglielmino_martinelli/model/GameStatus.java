package it.polimi.guglielmino_martinelli.model;

import it.polimi.guglielmino_martinelli.Coordinates;
import it.polimi.guglielmino_martinelli.events.AskWhichSheepEvent;
import it.polimi.guglielmino_martinelli.events.EnableBuyCardEvent;
import it.polimi.guglielmino_martinelli.events.EnableMoveSheepEvent;
import it.polimi.guglielmino_martinelli.events.GenericEvent;
import it.polimi.guglielmino_martinelli.events.InitialCostsSettedEvent;
import it.polimi.guglielmino_martinelli.events.MoveWhiteSheepEvent;
import it.polimi.guglielmino_martinelli.events.OccupiedRoadEvent;
import it.polimi.guglielmino_martinelli.events.PlaceFencesEvent;
import it.polimi.guglielmino_martinelli.events.PlayerNameSettedEvent;
import it.polimi.guglielmino_martinelli.events.PlayersNamesInsertedEvent;
import it.polimi.guglielmino_martinelli.events.PlayersNamesSettedEvent;
import it.polimi.guglielmino_martinelli.events.UpdateDiceResultEvent;
import it.polimi.guglielmino_martinelli.events.UpdateInstructionEvent;

import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

/**
 * Questa classe permette la memorizzazione dello stato generale del gioco.
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */

public class GameStatus extends Observable implements Observer{

	/**
	 * Rappresenta la mappa del gioco.
	 */
	private GameMap gameMap;
	/**
	 * Indica il costo delle tessere Terreno acquistabili.
	 */
	private Map<LandType, Integer> cardsCurrentCost;
	/**
	 * Rappresenta il numero di recinti disponibili.
	 */
	private int fencesAvailable;
	/**
	 * Array contenente tutti i giocatori.
	 */
	private Map<Integer, Player> players;
	/**
	 * Variabile che contiene il numero di giocatori ammessi a questa partita.
	 */
	private int numberOfPlayers;
	/**
	 * Indice del primo giocatore.
	 */
	private int starterPlayerIndex;
	/**
	 * Variabile che indica l'inizio della fase finale.
	 */
	private boolean finalPhase;
	/**
	 * Indice del giocatore corrente.
	 */
	private int currentPlayerIndex;
	/**
	 * Indice del pastore corrente.
	 */
	private int currentSheperdIndex;
	/**
	 * Varlore del dado;
	 */
	private int diceResult;
	/**
	 * Numero di azioni rimaste al giocatore.
	 */
	private int actionsLeft;
	/**
	 * Variabile che indica se è già stato scelto il pastore da usare in questo turno.
	 */
	private boolean sheperdSelected;
	/**
	 * Variabile che indica se il giocatore vuole muovere la pecora nera, quando in una regione ci sono sia le pecore bianche che quella nera.
	 */
	private boolean wantsToMoveBlack=false;
	/**
	 * Variabile boolean che indica la possibilità (true) o meno (false) di muovere una pecora.
	 */
	private boolean moveSheep;
	/**
	 * Variabile boolean che indica la possibilità (true) o meno (false) di comprare una tessera terreno.
	 */
	private boolean buyCard;

	private Region regionClicked;

	public GameStatus() {
		super();
		this.gameMap = new GameMap();
		//Dico che GameStatus osserva l'oggetto gameMap.
		gameMap.addObserver(this);

		this.cardsCurrentCost = new HashMap<LandType, Integer>();
		this.fencesAvailable = 32;
		this.players = new HashMap<Integer, Player>();
		this.finalPhase = false;
		this.currentSheperdIndex = 1;
		this.actionsLeft = 3;
		this.sheperdSelected = false;
		this.moveSheep = true;
		this.buyCard = true;
	}

	/**
	 * Procedura che restituisce l'indice del prossimo giocatore.
	 * @return indice del prossimo giocatore.
	 */
	private void nextPlayerIndex(){
		if(currentPlayerIndex < numberOfPlayers){
			currentPlayerIndex += 1;
		}else{
			currentPlayerIndex = 1;
		}
	}

	/**
	 * Inserisco il nome di un giocatore nel model.
	 * @param index indice del giocatore.
	 * @param player oggetto giocatore con i suoi dati.
	 */
	public void insertPlayer(int index, Player player) {
		players.put(index, player);
		
		PlayerNameSettedEvent event = new PlayerNameSettedEvent(player.getName());
		setChanged();
		notifyObservers(event);
	}

	/**
	 * Inserisco tutti i giocatori nel model.
	 * @param arg evento contenente la lista dei nomi dei giocatori.
	 */
	public void insertPlayers(PlayersNamesInsertedEvent arg) {
		int i = 1;
		numberOfPlayers = arg.getNames().size();

		for(String name : arg.getNames()){
			Player player = new Player(name, moneyPerPlayer(), i);
			player.addObserver(this);
			players.put(i, player);
			i++;
		}
		PlayersNamesSettedEvent event = new PlayersNamesSettedEvent();
		setChanged();
		notifyObservers(event);
	}

	/**
	 * Assegno la tessera terreno di tipo tile ad un giocatore.
	 * @param tile tipo di tessera terreno.
	 * @param playerIndex indice del giocatore.
	 */
	public void assignCardToPlayer(LandType tile, int playerIndex){
		
		//Setto il valore corretto nel model.
		players.get(playerIndex).getCardsOwned().put(tile, players.get(playerIndex).getCardsOwned().get(tile) + 1);
		
		CardsChangedEvent event = new CardsChangedEvent(players.get(playerIndex).getCardsOwned());
		setChanged();
		notifyObservers(event);
	}

	/**
	 * Procedura che stabilisce i danari iniziali di ciascun giocatore in base
	 * al numero di giocatori.
	 * @return Danari da assegnare a ciascun giocatore.
	 */
	public int moneyPerPlayer() {

		if (numberOfPlayers > 2) {
			return 20;
		}
		return 30;
	}

	/**
	 * Settaggio dei costi iniziali delle tessere terreno.
	 * @return HashMap con corrispondenze tra tipo di terreno e costo.
	 */
	public void setInitialCardsCosts(){
		for(LandType x : LandType.values()){
			if(!(x.equals(LandType.SHEEPSBURG))){
				this.cardsCurrentCost.put(x, 0);
			}
		}

		//Creo l'evento di inizializzazione dei costi iniziali delle tessere.
		InitialCostsSettedEvent event = new InitialCostsSettedEvent();

		//Segnalo l'evento al server.
		setChanged();
		notifyObservers(event);
	}

	public GameMap getGameMap() {
		return gameMap;
	}

	public Map<LandType, Integer> getCardsCurrentCost() {
		return cardsCurrentCost;
	}
	
	public void updateCardsCurrentCost(LandType tile){
		cardsCurrentCost.put(tile, cardsCurrentCost.get(tile) + 1);
		
		UpdateCardsCurrentCostEvent event = new UpdateCardsCurrentCostEvent(cardsCurrentCost);
		setChanged();
		notifyObservers(event);
	}

	public int getFencesAvailable() {
		return fencesAvailable;
	}

	public void setFencesAvailable(int fencesAvailable, Coordinates xy) {
		if(this.fencesAvailable>12){
			PlaceFencesEvent event = new PlaceFencesEvent(xy, false);
			setChanged();
			notifyObservers(event);
		}else{
			finalPhase=true;
			PlaceFencesEvent event = new PlaceFencesEvent(xy, true);
			setChanged();
			notifyObservers(event);
		}
		
		this.fencesAvailable = fencesAvailable;
	}

	public Map<Integer, Player> getPlayers() {
		return players;
	}

	public int getNumberOfPlayers() {
		return numberOfPlayers;
	}

	public void setNumberOfPlayers(int numberOfPlayers) {
		this.numberOfPlayers = numberOfPlayers;
	}

	public int getStarterPlayerIndex() {
		return starterPlayerIndex;
	}

	public void setStarterPlayerIndex(int starterPlayerIndex) {
		this.starterPlayerIndex = starterPlayerIndex;
	}

	public boolean isFinalPhase() {
		return finalPhase;
	}

	public void setFinalPhase(boolean finalPhase) {
		this.finalPhase = finalPhase;
	}

	public int getCurrentPlayerIndex() {
		return currentPlayerIndex;
	}

	public void setCurrentPlayerIndex(int currentPlayerIndex) {
		this.currentPlayerIndex = currentPlayerIndex;
		
		PlayerChangedEvent event = new PlayerChangedEvent(players.get(currentPlayerIndex));
		setChanged();
		notifyObservers(event);
	}

	public int getCurrentSheperdIndex() {
		return currentSheperdIndex;
	}

	public void setCurrentSheperdIndex(int currentSheperdIndex) {
		this.currentSheperdIndex = currentSheperdIndex;
	}

	public void updateInstruction(String instruction, int playerIndex) {

		UpdateInstructionEvent event= new UpdateInstructionEvent(instruction, playerIndex);
		setChanged();
		notifyObservers(event);
	}

	public void setDiceResult(int diceResult) {
		this.diceResult = diceResult;

		UpdateDiceResultEvent event = new UpdateDiceResultEvent(diceResult);
		setChanged();
		notifyObservers(event);
	}

	public int getDiceResult(){
		return diceResult;
	}

	public int getActionsLeft() {
		return actionsLeft;
	}

	public void setActionsLeft(int actionsLeft) {
		this.actionsLeft = actionsLeft;
		
		if(actionsLeft == 0){
			updateInstruction("Attendi il tuo turno", currentPlayerIndex);
			this.actionsLeft = 3;
			this.sheperdSelected = false;
			
			nextPlayerIndex();
			
			PlayerChangedEvent event = new PlayerChangedEvent(players.get(currentPlayerIndex));
			setChanged();
			notifyObservers(event);		
			
			setCanMoveSheep(true);
			setCanBuyCard(true);
		}
	}

	public boolean isSheperdSelected() {
		return sheperdSelected;
	}

	public void setSheperdSelected(boolean sheperdSelected) {
		this.sheperdSelected = sheperdSelected;
	}

	public boolean isWantsToMoveBlack() {
		return wantsToMoveBlack;
	}

	public void setWantsToMoveBlack(boolean wantsToMoveBlack) {
		this.wantsToMoveBlack = wantsToMoveBlack;
	}
	
	public boolean canMoveSheep() {
		return moveSheep;
	}

	public void setCanMoveSheep(boolean moveSheep) {
		this.moveSheep = moveSheep;
		
		EnableMoveSheepEvent event = new EnableMoveSheepEvent(moveSheep);
		setChanged();
		notifyObservers(event);
	}

	public boolean canBuyCard() {
		return buyCard;
	}

	public void setCanBuyCard(boolean buyCard) {
		this.buyCard = buyCard;
		
		EnableBuyCardEvent event = new EnableBuyCardEvent(buyCard);
		setChanged();
		notifyObservers(event);
	}

	public Region getRegionClicked() {
		return regionClicked;
	}

	public void setRegionClicked(Region regionClicked) {
		this.regionClicked = regionClicked;
	}
	
	public void askWhichSheep(){
		AskWhichSheepEvent event = new AskWhichSheepEvent();
		event.setPlayerIndex(currentPlayerIndex);
		setChanged();
		notifyObservers(event);
	}
	
	public void showFinalScreen(Map<Integer, Player> players){
		FinalScreenEvent event = new FinalScreenEvent(players);
		setChanged();
		notifyObservers(event);
	}

	@Override
	public String toString() {
		return "Grafo [" + gameMap
				+ "] \nCosto delle tessere terreno [" + cardsCurrentCost
				+ "] \nNumero di recinti disponibili [" + fencesAvailable
				+ "] \nElenco di giocatori [" + players + "]";
	}

	@Override
	public void update(Observable o, Object arg) {

		switch(((GenericEvent)arg).getType()){

		case MOVE_WHITE_SHEEP_EVENT:
			MoveWhiteSheepEvent event = (MoveWhiteSheepEvent)arg;
			event.setSource(regionClicked.getXY());
			setChanged();
			notifyObservers(event);
			break;

		case OCCUPIED_ROAD_EVENT:
			((OccupiedRoadEvent)arg).setPlayerIndex(currentPlayerIndex);
			setChanged();
			notifyObservers((OccupiedRoadEvent)arg);
			break;

		default:
			setChanged();
			notifyObservers((GenericEvent)arg);
			break;
		}
	}
}