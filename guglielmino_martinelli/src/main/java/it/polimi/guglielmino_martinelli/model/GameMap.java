package it.polimi.guglielmino_martinelli.model;

import it.polimi.guglielmino_martinelli.Coordinates;
import it.polimi.guglielmino_martinelli.Main;
import it.polimi.guglielmino_martinelli.events.GenericEvent;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;
import java.util.logging.Level;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jgrapht.UndirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;

/**
 * Classe per la memorizzazione delle regioni, strade e relativo grafo.
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public class GameMap extends Observable implements Observer {

	/**
	 * Grafo non orientato per memorizzare la mappa del gioco.
	 */
	private UndirectedGraph<GraphNode, DefaultEdge> graph;
	/**
	 * HashMap dove viene memorizzata la corrispondenza id, regione.
	 */
	private Map<String, Region> allRegions;
	/**
	 * HashMap dove viene memorizzata la corrispondenza id, strada.
	 */
	private Map<String, Road> allRoads;

	public GameMap() {
		super();
		this.allRegions = new HashMap<String, Region>();
		this.allRoads = new HashMap<String, Road>();
	}

	/**
	 * Funzione per la creazione del grafo non orientato che rappresenta la mappa del gioco.
	 * @return grafo non orientato con regioni e strade come vertici.
	 */
	public void createGraph(){
		UndirectedGraph<GraphNode, DefaultEdge> g = new SimpleGraph<GraphNode, DefaultEdge>(DefaultEdge.class);

		SAXBuilder builder = new SAXBuilder();
		InputStream xmlFile = this.getClass().getResourceAsStream("/sheepland_map.xml");

		try{
			Document document = (Document)builder.build(xmlFile);
			Element rootNode = document.getRootElement();

			addRegionsToMap(g, rootNode);
			addRoadsToMap(g, rootNode);
			addEdgesToMap(g, rootNode);

		}catch(IOException | JDOMException e){
			Main.LOGGER.log(Level.SEVERE, "Errore", e);
		}
		this.graph = g;
	}

	/**
	 * Funzione che aggiunge le regioni al grafo e all'hashmap.
	 * @param g grafo non orientato.
	 * @param rootNode nodo radice del file xml da cui viene fatto il parsing.
	 */
	private void addRegionsToMap(UndirectedGraph<GraphNode, DefaultEdge> g, Element rootNode){
		//Lista testuale regioni.
		List<?> regions = rootNode.getChildren("region");
		//Iterazione, creo le regioni e le aggiungo al grafo, le aggiungo anche nell'hashmap.
		for(Object x : regions){

			Region r = new Region();

			//Dico che la GameMap osserva l'oggetto regione.
			r.addObserver(this);

			//Aggiungo il tipo alla regione
			r.setType(LandType.parseType(((Element)x).getChildText("type")));

			if(r.getType()==LandType.SHEEPSBURG){
				r.initSheepsburg();
			}else{
				r.initRegion();
			}
			r.setRGB(Integer.parseInt(((Element)x).getChildText("rgb")));
			r.setXY(new Coordinates(Integer.parseInt(((Element)x).getChildText("x")), Integer.parseInt(((Element)x).getChildText("y"))));

			g.addVertex(r);
			allRegions.put(((Element)x).getChildText("id"), r);
		}
	}

	/**
	 * Funzione che aggiunge le strade al grafo e all'hashmap.
	 * @param g grafo non orientato.
	 * @param rootNode nodo radice del file xml da cui viene fatto il parsing.
	 */
	private void addRoadsToMap(UndirectedGraph<GraphNode, DefaultEdge> g, Element rootNode){
		//Lista testuale strade.
		List<?> roads = rootNode.getChildren("road");
		//Iterazione, creo le strade e le aggiungo al grafo, le aggiungo anche nell'hashmap.
		for(Object x : roads){

			Road r = new Road();

			//Dico che la GameMap osserva l'oggetto strada.
			r.addObserver(this);

			//Aggiungo il numero alla starda.
			r.setNumber(Integer.parseInt(((Element)x).getChildText("number")));

			//Aggiungo le coordinate alla strada.
			Coordinates xy = new Coordinates();
			xy.setX(Integer.parseInt(((Element)x).getChildText("x")));
			xy.setY(Integer.parseInt(((Element)x).getChildText("y")));
			r.setXY(xy);

			//Aggiungo la strada al grafo.
			g.addVertex(r);

			//Aggiungo la strada nell'hashmap di strade.
			allRoads.put(((Element)x).getChildText("id"), r);
		}
	}

	/**
	 * Funzione che aggiunge i collegamenti al grafo.
	 * @param g grafo non orientato.
	 * @param rootNode nodo radice del file xml da cui viene fatto il parsing.
	 */
	private void addEdgesToMap(UndirectedGraph<GraphNode, DefaultEdge> g, Element rootNode){
		//Lista testuale collegamenti tra strade.
		List<?> roadToRoad = rootNode.getChildren("roadtoroad");
		//Iterazione, aggiungo i collegamenti al grafo.
		for(Object x : roadToRoad){
			g.addEdge(allRoads.get(((Element)x).getChildText("el0")), allRoads.get(((Element)x).getChildText("el1")));
		}

		//Lista testuale collegamenti tra regioni e strade.
		List<?> regionToRoad = rootNode.getChildren("regiontoroad");
		//Iterazione, aggiungo i collegamenti al grafo.
		for(Object x : regionToRoad){
			g.addEdge(allRegions.get(((Element)x).getChildText("el0")), allRoads.get(((Element)x).getChildText("el1")));
		}
	}

	/**
	 * Funzione che data in ingresso una regione o una strada restituisce le strade adiacenti ad essa.
	 * @param r regione di cui voglio sapere le strade adiacenti.
	 * @return HashSet di strade.
	 */
	public Set<Road> getAdjacentRoads(GraphNode r){
		Set<Road> adjacentRoads = new HashSet<Road>();

		//Iterazione della HashMap contenente tutte le strade.
		for(Entry<String, Road> entry : allRoads.entrySet()){

			//Controllo se la regione this è collegata alla strada corrente entry.
			if(graph.containsEdge(r, entry.getValue())){

				//Se esiste il collegamento aggiungo la entry alla lista delle adiacenze che poi restituisco.
				adjacentRoads.add(entry.getValue());
			}
		}
		return adjacentRoads;
	}

	/**
	 * Funzione che data in ingresso una strada restituisce le regioni adiacenti ad essa.
	 * @param r strada di cui voglio sapere le regioni adicenti.
	 * @return HashSet di regioni.
	 */
	public Set<Region> getAdjacentRegions(Road r){
		Set<Region> adjacentRegions = new HashSet<Region>();

		//Iterazione della HashMap contenente tutte le regioni.
		for(Entry<String, Region> entry : allRegions.entrySet()){

			//Controllo se la strada this è collegata alla regione corrente entry.
			if(graph.containsEdge(r, entry.getValue())){

				//Se esiste il collegamento aggiungo la entry alla lista delle adiacenze che poi restituisco.
				adjacentRegions.add(entry.getValue());
			}
		}
		return adjacentRegions;
	}

	/**
	 * Funzione che data una regione di partenza e una via di passaggio trova la regione di arrivo.
	 * @param way via di passaggio.
	 * @param sourceRegion regione di partenza.
	 * @return regione di arrivo.
	 */
	public Region getCorrectDestination(Road way, Region sourceRegion){
		
		//Ricavo l'insieme di regioni adiacenti alla strada passata come parametro.
		Set<Region> destinationRegions = this.getAdjacentRegions(way);

		//Scorro tutte le possibili regioni di destinazione.
		for(Region destinationRegion : destinationRegions){

			//Controllo che la regione di destinazione non coincida con la regione di partenza.
			if(!(sourceRegion.equals(destinationRegion))){
				return destinationRegion;
			}
		}
		//Caso limite, se la strada è adiacente ad una sola regione, la destinazione coincide con la partenza.
		return sourceRegion;
	}

	/**
	 * Funzione che restituisce il totale delle pecore per ogni tipo di terreno.
	 * @return HashMap con l'associazione tipo di terreno, numero pecore.
	 */
	public Map<LandType, Integer> sheepPerLandType(){
		Map<LandType, Integer> count = new HashMap<LandType, Integer>();
		int numberOfSheep;
		
		for(LandType land : LandType.values()){
			numberOfSheep = 0;

			for(Region r : allRegions.values()){
				if(land.equals(r.getType())){
					numberOfSheep += r.getWhiteSheep();
				}
			}
			count.put(land, numberOfSheep);
		}
		return count;
	}

	/**
	 * Trova la regione occupata dalla pecora nera.
	 * @return oggetto regione.
	 */
	public Region findBlackSheepRegion(){

		//Scorro tutte le regioni della mappa.
		for(Entry<String, Region> entry : allRegions.entrySet()){
			Region r = entry.getValue();

			//Controllo se nella regione in esame c'è la pecora nera.
			if(r.getBlackSheep()){
				//Ritorno la regione con la pecora nera.
				return r;
			}
		}
		//Non ho trovato la pecora nera (caso impossibile stando alle regole del gioco).
		return null;
	}

	public UndirectedGraph<GraphNode, DefaultEdge> getGraph() {
		return graph;
	}

	public Map<String, Region> getAllRegions() {
		return allRegions;
	}

	public Map<String, Road> getAllRoads() {
		return allRoads;
	}

	@Override
	public String toString() {
		return "GameMap [graph=" + graph + ", allRegions=" + allRegions
				+ ", allRoads=" + allRoads + "]";
	}

	@Override
	public void update(Observable o, Object arg) {
		setChanged();
		notifyObservers((GenericEvent)arg);
	}
}