package it.polimi.guglielmino_martinelli.model;

import it.polimi.guglielmino_martinelli.events.GenericEvent;

/**
 * Evento di aggiunta di una determinata regione al grafo.
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public class AddRegionEvent extends GenericEvent{

	private static final long serialVersionUID = 4154806902736229779L;
	/**
	 * Regione aggiunta.
	 */
	private Region region;

	public AddRegionEvent(Region region){
		super(EventType.ADD_REGION_EVENT);
		this.region = region;
	}

	public Region getRegion(){
		return region;
	}
}