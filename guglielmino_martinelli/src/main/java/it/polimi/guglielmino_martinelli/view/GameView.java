package it.polimi.guglielmino_martinelli.view;

import it.polimi.guglielmino_martinelli.Coordinates;
import it.polimi.guglielmino_martinelli.Main;
import it.polimi.guglielmino_martinelli.events.ChosedBuyCardEvent;
import it.polimi.guglielmino_martinelli.events.ChosedMoveSheepEvent;
import it.polimi.guglielmino_martinelli.events.ChosedMoveSheperdEvent;
import it.polimi.guglielmino_martinelli.events.ChosedRegionEvent;
import it.polimi.guglielmino_martinelli.events.ChosedRoadEvent;
import it.polimi.guglielmino_martinelli.events.ChosedSheperdEvent;
import it.polimi.guglielmino_martinelli.events.EnableBuyCardEvent;
import it.polimi.guglielmino_martinelli.events.EnableMoveSheepEvent;
import it.polimi.guglielmino_martinelli.events.GenericEvent;
import it.polimi.guglielmino_martinelli.events.InitialPhaseEvent;
import it.polimi.guglielmino_martinelli.events.MoveBlackSheepEvent;
import it.polimi.guglielmino_martinelli.events.MoveWhiteSheepEvent;
import it.polimi.guglielmino_martinelli.events.OccupiedRoadEvent;
import it.polimi.guglielmino_martinelli.events.PlaceFencesEvent;
import it.polimi.guglielmino_martinelli.events.UpdateDiceResultEvent;
import it.polimi.guglielmino_martinelli.events.UpdateInstructionEvent;
import it.polimi.guglielmino_martinelli.events.UpdateMoneyEvent;
import it.polimi.guglielmino_martinelli.events.WantsToMoveBlackEvent;
import it.polimi.guglielmino_martinelli.model.AddRegionEvent;
import it.polimi.guglielmino_martinelli.model.AddRoadEvent;
import it.polimi.guglielmino_martinelli.model.CardsChangedEvent;
import it.polimi.guglielmino_martinelli.model.ChosedCardEvent;
import it.polimi.guglielmino_martinelli.model.FinalScreenEvent;
import it.polimi.guglielmino_martinelli.model.LandType;
import it.polimi.guglielmino_martinelli.model.Player;
import it.polimi.guglielmino_martinelli.model.PlayerChangedEvent;
import it.polimi.guglielmino_martinelli.model.Region;
import it.polimi.guglielmino_martinelli.model.Road;
import it.polimi.guglielmino_martinelli.model.UpdateCardsCurrentCostEvent;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/**
 * Finestra di gioco.
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public class GameView extends View implements Observer{

	/**
	 * Finestra prncipale.
	 */
	private JFrame gameScreen;
	/**
	 * Pannello per la mappa di gioco.
	 */
	private JPanel mapPanel;
	/**
	 * Gestore dei livelli.
	 */
	private JLayeredPane layeredPane;
	/**
	 * Pannello per la scelta di quale pecora muovere.
	 */
	private JPanel chooseWhichSheepPanel;
	/**
	 * Bottone per la scelta della pecora bianca.
	 */
	private JButton moveWhite;
	/**
	 * Bottone per la scelta della pecora nera.
	 */
	private JButton moveBlack;
	/**
	 * Pannello per le tessere terreno.
	 */
	private JPanel cardsPanel;
	/**
	 * Pannello di controllo del giocatore.
	 */
	private JPanel controlPanel;
	/**
	 * Oggetto BufferedImage per la mappa in scala di grigi.
	 */
	private BufferedImage greyMapBuffered;
	/**
	 * Lista che contiene tutti i bottoni relativi alle strade sulla mappa.
	 */
	private Map<Coordinates, JButton> roads;
	/**
	 * Label che indica il numero di recinti disponibili
	 */
	private JLabel fencesNumber;
	/**
	 * Lista che contiene tutti i bottoni relativi alle tessere terreno.
	 */
	private Map<LandType, JButton> cards;
	/**
	 * HashMap che contiene tutte le label relative alle tessere terreno possedute dal giocatore.
	 */
	private Map<LandType, JLabel> cardsOwned;
	/**
	 * Valore assunto dal dado dopo il lacio all'inizio del turno
	 */
	private JLabel dice;
	/**
	 * Pannello con i dati del giocatore
	 */
	private JPanel playerPanel;
	/**
	 * Bottone per scegliere la mossa "Muovi pastore"
	 */
	private JButton moveSheperd;
	/**
	 * Bottone per scegliere la mossa "Muovi pecora"
	 */
	private JButton moveSheep;
	/**
	 * Bottone per scegliere la mossa "Compra tessera"
	 */
	private JButton buyCard;
	/**
	 * Istruzioni per l'utente.
	 */
	private JLabel instruction;
	/**
	 * Nome dell'utente.
	 */
	private JLabel myName;
	/**
	 * Danari dell'utente.
	 */
	private JLabel myMoney;
	/**
	 * Label che ospiterà l'immagine delle tessere terreno in miniatura (quelle del giocatore).
	 */
	private JLabel myCards;
	/**
	 * Listener della mappa di gioco.
	 */
	private MapListener mapListener;
	/**
	 * Lista di label per le pedine pastore.
	 */
	private List<Sheperd> sheperds;
	/**
	 * Label per la pecora nera.
	 */
	private JLabel blackSheep;
	/**
	 * Lista di label per le pecore bianche.
	 */
	private List<JLabel> whiteSheep;
	/**
	 * Label usata per il movimento delle pecore.
	 */
	private JLabel whiteSheepToMove;
	/**
	 * Label usata come indicatore grafico del giocatore.
	 */
	private JLabel playerIndicator;


	public GameView(){

		/*==========FINESTRA DI GIOCO==========*/

		//Creo la finestra gameScreen (JFrame).
		gameScreen = new JFrame("Sheepland");
		gameScreen.setSize(940, 695);
		gameScreen.setResizable(false);
		gameScreen.setLocationRelativeTo(null);
		gameScreen.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		gameScreen.setLayout(null);

		//Creo il pannello della mappa di gioco.
		mapPanel = new BackgroundPanel(this.getClass().getResource("/img/Game_Board.png"));
		mapPanel.setBounds(0, 0, 470, 675);

		//Imposto un layer alla mappa pe poter visualizza il pannello per la selezione di quale pecora muovere.
		layeredPane = new JLayeredPane();
		layeredPane.setBounds(0, 0, 470, 675);
		layeredPane.add(mapPanel, Integer.valueOf(0));

		//Creo il pannello con le tessere e i loro costi.
		cardsPanel = new BackgroundPanel(this.getClass().getResource("/img/CardsPanel.png"));
		cardsPanel.setBounds(470, 0, 125, 675);
		cardsPanel.setLayout(null);

		//Creo il pannello di controllo del giocatore.
		controlPanel = new BackgroundPanel(this.getClass().getResource("/img/ControlPanel.png"));
		controlPanel.setBounds(595, 0, 345, 675);
		controlPanel.setLayout(null);

		playerPanel = new JPanel();
		playerPanel.setBounds(0, 380, 345, 295);
		playerPanel.setLayout(null);
		playerPanel.setOpaque(false);

		//Creo l'immagine in scala di grigi della mappa in memoria.
		createGreyMap();

		//Aggiungo tutti gli elementi alla finestra.
		gameScreen.add(layeredPane);
		gameScreen.add(cardsPanel);
		gameScreen.add(controlPanel);
		controlPanel.add(playerPanel);

		chooseWhichSheepPanel = new BackgroundPanel(this.getClass().getResource("/img/whichSheep_screen.png"));
		chooseWhichSheepPanel.setBounds(85, 200, 300, 248);
		chooseWhichSheepPanel.setOpaque(false);
		chooseWhichSheepPanel.setVisible(false);
		chooseWhichSheepPanel.setLayout(null);
		layeredPane.add(chooseWhichSheepPanel, Integer.valueOf(1));

		moveWhite = new JButton(new ImageIcon(this.getClass().getResource("/img/white_sheep_big.png")));
		moveWhite.setBounds(50, 75, 80, 72);
		moveWhite.setBorder(null);
		moveWhite.setContentAreaFilled(false);
		moveWhite.setVisible(false);
		moveWhite.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				chooseWhichSheepPanel.setVisible(false);
				moveWhite.setVisible(false);
				moveBlack.setVisible(false);

				WantsToMoveBlackEvent event = new WantsToMoveBlackEvent(false);
				setChanged();
				notifyObservers(event);
			}
		});

		chooseWhichSheepPanel.add(moveWhite);

		moveBlack = new JButton(new ImageIcon(this.getClass().getResource("/img/black_sheep_big.png")));
		moveBlack.setBounds(164, 75, 86, 72);
		moveBlack.setBorder(null);
		moveBlack.setContentAreaFilled(false);
		moveBlack.setVisible(false);
		moveBlack.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				chooseWhichSheepPanel.setVisible(false);
				moveWhite.setVisible(false);
				moveBlack.setVisible(false);

				WantsToMoveBlackEvent event = new WantsToMoveBlackEvent(true);
				setChanged();
				notifyObservers(event);	
			}
		});

		chooseWhichSheepPanel.add(moveBlack);


		//Creo il listener del pannello della mappa per rilevare le regioni cliccate.
		mapListener = new MapListener();

		roads = new HashMap<Coordinates, JButton>();

		whiteSheep = new ArrayList<JLabel>();

		fencesNumber = new JLabel(new ImageIcon(this.getClass().getResource("/img/FencesNumber.png")));
		fencesNumber.setBounds((cardsPanel.getWidth()/2)-(75/2), 25, 75, 75);
		fencesNumber.setText(""+20);
		fencesNumber.setIconTextGap(-60);
		fencesNumber.setFont(new Font(null, Font.BOLD, 30));
		fencesNumber.setForeground(new Color(240,240,240));
		cardsPanel.add(fencesNumber);

		//Creo i bottoni per le tessere terreno.
		cards = new HashMap<LandType, JButton>();

		JButton b = new JButton(new ImageIcon(this.getClass().getResource("/img/field.png")));
		b.setBounds((cardsPanel.getWidth()/2)-(100/2), (500/6)*(1) + 40, 100, 70);
		cards.put(LandType.FIELD, b);
		b = new JButton(new ImageIcon(this.getClass().getResource("/img/forest.png")));
		b.setBounds((cardsPanel.getWidth()/2)-(100/2), (500/6)*(2) + 40, 100, 70);
		cards.put(LandType.FOREST, b);
		b =  new JButton(new ImageIcon(this.getClass().getResource("/img/heath.png")));
		b.setBounds((cardsPanel.getWidth()/2)-(100/2), (500/6)*(3) + 40, 100, 70);
		cards.put(LandType.HEATH, b);
		b = new JButton(new ImageIcon(this.getClass().getResource("/img/lake.png")));
		b.setBounds((cardsPanel.getWidth()/2)-(100/2), (500/6)*(4) + 40, 100, 70);
		cards.put(LandType.LAKE, b);
		b = new JButton(new ImageIcon(this.getClass().getResource("/img/mountain.png")));
		b.setBounds((cardsPanel.getWidth()/2)-(100/2), (500/6)*(5) + 40, 100, 70);
		cards.put(LandType.MOUNTAIN, b);
		b = new JButton(new ImageIcon(this.getClass().getResource("/img/plain.png")));
		b.setBounds((cardsPanel.getWidth()/2)-(100/2), (500/6)*(6) + 40, 100, 70);
		cards.put(LandType.PLAIN, b);

		for(JButton x : cards.values()){
			cardsPanel.add(x);
		}

		moveSheperd = new JButton(new ImageIcon(this.getClass().getResource("/img/moveSheperd.png")));
		moveSheperd.setBounds(63, 60, 210, 50);
		moveSheperd.setEnabled(false);
		moveSheperd.setBorder(null);
		moveSheperd.setContentAreaFilled(false);
		moveSheperd.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e){
				ChosedMoveSheperdEvent event = new ChosedMoveSheperdEvent();
				setChanged();
				notifyObservers(event);	
			}
		});

		moveSheep = new JButton(new ImageIcon(this.getClass().getResource("/img/moveSheep.png")));
		moveSheep.setBounds(63, 120, 210, 50);
		moveSheep.setEnabled(false);
		moveSheep.setBorder(null);
		moveSheep.setContentAreaFilled(false);
		moveSheep.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e){
				enableRegions(true);
				ChosedMoveSheepEvent event = new ChosedMoveSheepEvent();
				setChanged();
				notifyObservers(event);	
			}
		});

		buyCard = new JButton(new ImageIcon(this.getClass().getResource("/img/buyCard.png")));
		buyCard.setBounds(63, 180, 210, 50);
		buyCard.setEnabled(false);
		buyCard.setBorder(null);
		buyCard.setContentAreaFilled(false);
		buyCard.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e){
				ChosedBuyCardEvent event = new ChosedBuyCardEvent();
				setChanged();
				notifyObservers(event);	
			}
		});

		controlPanel.add(moveSheperd);
		controlPanel.add(moveSheep);
		controlPanel.add(buyCard);

		dice = new JLabel();
		dice.setBorder(null);
		dice.setBounds(0, 280, 345, 70);
		dice.setEnabled(false);
		dice.setHorizontalAlignment(SwingConstants.CENTER);

		controlPanel.add(dice);

		Color c = new Color(80,56,18);

		instruction = new JLabel("Attendi il tuo turno");
		instruction.setBounds(0, 20, 345, 50);
		instruction.setHorizontalAlignment(SwingConstants.CENTER);
		instruction.setFont(new Font(null, Font.BOLD, 18));
		instruction.setForeground(c);

		playerPanel.add(instruction);

		playerIndicator = new JLabel();
		playerIndicator.setBounds(0, 0, 345, 25);
		playerIndicator.setHorizontalAlignment(SwingConstants.CENTER);

		playerPanel.add(playerIndicator);

		myName = new JLabel();
		myName.setBounds(0, 90, 345, 20);
		myName.setHorizontalAlignment(SwingConstants.CENTER);
		myName.setFont(new Font(null, Font.BOLD, 15));
		myName.setForeground(c);

		playerPanel.add(myName);

		myMoney = new JLabel();
		myMoney.setBounds(0, 120, 345, 20);
		myMoney.setHorizontalAlignment(SwingConstants.CENTER);
		myMoney.setFont(new Font(null, Font.BOLD, 15));
		myMoney.setForeground(c);

		playerPanel.add(myMoney);

		myCards = new JLabel();
		ImageIcon cardsImg = new ImageIcon(this.getClass().getResource("/img/cardsOwned.png"));
		myCards.setIcon(cardsImg);
		myCards.setBounds(0, 160, 345, 50);
		myCards.setHorizontalAlignment(SwingConstants.CENTER);

		playerPanel.add(myCards);

		//Creo le label che indicano le tessere terreno possedute
		cardsOwned = new HashMap<LandType, JLabel>();
		for(LandType x : LandType.values()){
			if(!(x.equals(LandType.SHEEPSBURG))){
				JLabel label = new JLabel("0");
				cardsOwned.put(x, label);
				label.setBounds(5+(47)*cardsOwned.size(), 210, 20, 20);
				label.setFont(new Font(null, Font.BOLD, 13));
				label.setForeground(c);
				playerPanel.add(label);
			}
		}
		new ImageIcon(this.getClass().getResource("/img/blue_sheperd_round.png"));
		//Creo la lista di pastori JLabel prendendo i corrispondenti file immagine.
		sheperds = new ArrayList<Sheperd>();
		sheperds.add(new Sheperd(new ImageIcon(this.getClass().getResource("/img/blue_sheperd_round.png")), 1, 1));
		sheperds.add(new Sheperd(new ImageIcon(this.getClass().getResource("/img/red_sheperd_round.png")), 2, 1));
		sheperds.add(new Sheperd(new ImageIcon(this.getClass().getResource("/img/green_sheperd_round.png")), 3, 1));
		sheperds.add(new Sheperd(new ImageIcon(this.getClass().getResource("/img/yellow_sheperd_round.png")), 4, 1));
		sheperds.add(new Sheperd(new ImageIcon(this.getClass().getResource("/img/blue_sheperd_round.png")), 1, 2));
		sheperds.add(new Sheperd(new ImageIcon(this.getClass().getResource("/img/red_sheperd_round.png")), 2, 2));

		//Setto le impostazioni iniziali per i pastori, li nascondo e li metto nell'angolo in alto a sinistra.
		for(final JLabel sheperd : sheperds){

			sheperd.setVisible(false);
			sheperd.setEnabled(false);
			sheperd.setBounds(235, 291, 25, 25);
			//Aggiungo i mouse listener alle label per renderle cliccabili dall'utente.
			sheperd.addMouseListener(new MouseListener(){

				@Override
				public void mouseClicked(MouseEvent e) {
					ChosedSheperdEvent event = new ChosedSheperdEvent(((Sheperd)sheperd).getSheperdIndex());
					event.setPlayerIndex(((Sheperd)sheperd).getPlayerIndex());
					setChanged();
					notifyObservers(event);
				}

				@Override
				public void mousePressed(MouseEvent e) {				
				}

				@Override
				public void mouseReleased(MouseEvent e) {
				}

				@Override
				public void mouseEntered(MouseEvent e) {
				}

				@Override
				public void mouseExited(MouseEvent e) {
				}

			});
			mapPanel.add(sheperd);
		}

		ImageIcon img = new ImageIcon(this.getClass().getResource("/img/white_sheep_to_move.png"));
		whiteSheepToMove = new JLabel(img);
		whiteSheepToMove.setBounds(0, 0, 27, 25);
		whiteSheepToMove.setVisible(false);
		mapPanel.add(whiteSheepToMove);
	}

	/**
	 * Carica in memoria la bufferedImage con la mappa in scala di grigi.
	 */
	public void createGreyMap(){
		Image greyMapImage = new ImageIcon(this.getClass().getResource("/img/Game_Board_Grey.png")).getImage();
		greyMapBuffered = new BufferedImage(greyMapImage.getWidth(null), greyMapImage.getHeight(null), BufferedImage.TYPE_INT_ARGB);
		Graphics g = greyMapBuffered.createGraphics();
		g.drawImage(greyMapImage, 0, 0, null);
		g.dispose();
	}

	/**
	 * Aggiunge un bottone strada sulla mappa.
	 * @param xy coordinate del centro della strada.
	 */
	public void drawRoadButton(final Road road){

		final Coordinates xy = road.getXY();

		//Creo il bottone per la strada.
		ImageIcon roadButton = new ImageIcon(this.getClass().getResource("/img/road_button.png"));
		final JButton rb = new JButton(roadButton);
		rb.setBounds(xy.getX(), xy.getY(), 25, 25);
		rb.setBorder(null);
		rb.setContentAreaFilled(false);
		rb.setEnabled(true);

		rb.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e){

				ChosedRoadEvent event = new ChosedRoadEvent(xy);
				setChanged();
				notifyObservers(event);	
			}
		});

		roads.put(xy, rb);

		mapPanel.add(rb);

		mapPanel.repaint();
	}

	/**
	 * Disegna le tessere terreno sulla schermata di gioco.
	 */
	public void drawGameCards(){
		for(final Entry<LandType, JButton> x : cards.entrySet()){
			x.getValue().setText(""+0);
			x.getValue().setIconTextGap(-50);
			x.getValue().setBorder(null);
			x.getValue().setContentAreaFilled(false);
			x.getValue().setFont(new Font(null, Font.BOLD, 40));
			x.getValue().setForeground(new Color(159,111,75));
			x.getValue().addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e){

					ChosedCardEvent event = new ChosedCardEvent(x.getKey());
					setChanged();
					notifyObservers(event);
				}
			});	
		}
		cardsPanel.repaint();
	}

	/**
	 * Aggiorna le informazioni del giocatore corrente.
	 */
	public void changePlayerInfo(Player player) {
		enableControlPanel(true);
		myName.setText("Giocatore: " + player.getName());
		myMoney.setText("Danari: " + player.getMoney());
		updateCardsOwned(player.getCardsOwned());
	}

	/**
	 * Aggiorno il valore di una tessera posseduta.
	 * @param tile tipo di tessera posseduta.
	 * @param newValue valore aggiornato.
	 */
	public void updateCardsOwned(Map<LandType, Integer> cardsOwned) {
		for(LandType x : cardsOwned.keySet()){
			this.cardsOwned.get(x).setText(""+cardsOwned.get(x));
		}
		controlPanel.repaint();
	}

	/**
	 * Realizza il cambio di istruzioni a video.
	 * @param txt testo dell'istruzione.
	 */
	public void updateInstruction(String txt, int index){
		if("Attendi il tuo turno".equals(txt)){
			enableControlPanel(false);
		}
		instruction.setText(txt);
		playerIndicator.setIcon(sheperds.get(index-1).getImg());
	}

	/**
	 * Procedura che abilita/disabilita le regioni cliccabili
	 * sul pannello della mappa di gioco a seconda del valore di enable.
	 * @param enable valore booleano.
	 */
	public void enableRegions(boolean enable){
		if(enable){
			mapPanel.addMouseListener(mapListener);
		}else{
			mapPanel.removeMouseListener(mapListener);
		}
	}

	/**
	 * Procedura che abilita/disabilta i controlli sul pannello
	 * di controllo delle azioni dell'utente a seconda del
	 * valore di enable.
	 * @param enable valore booleano.
	 */
	public void enableControlPanel(boolean enable){
		moveSheperd.setEnabled(enable);
		moveSheep.setEnabled(enable);
		buyCard.setEnabled(enable);
		dice.setEnabled(enable);
	}

	/**
	 * Realizza il cambio dei danari a video.
	 * @param money nuovo valore dei danari.
	 */
	private void updateMoney(int money){
		myMoney.setText("Danari: " + money);
	}

	/**
	 * Procedura che rappresenta a video sulla mappa le pecore bianche e la pecora nera.
	 * @param region regione in cui si deve visualizzare la pecora bianca/nera.
	 */
	private void drawAnimals(Region region){

		ImageIcon img = new ImageIcon(this.getClass().getResource("/img/white_sheep.png"));
		JLabel sheep = new JLabel(img);
		sheep.setBounds(region.getXY().getX(), region.getXY().getY(), 50, 25);
		sheep.setText("");
		sheep.setHorizontalTextPosition(SwingConstants.CENTER);
		whiteSheep.add(sheep);
		mapPanel.add(sheep);
		if((LandType.SHEEPSBURG).equals(region.getType())){
			sheep.setVisible(false);
			img = new ImageIcon(this.getClass().getResource("/img/black_sheep.png"));
			blackSheep = new JLabel(img);
			blackSheep.setBounds(region.getXY().getX()-18, region.getXY().getY()-18, 25, 25);
			mapPanel.add(blackSheep);		
		}
	}

	/**
	 * Animazione per il movimento del pastore che si posiziona sulla plancia di gioco.
	 * @param myIndex indice del giocatore.
	 * @param xy coordinate di destinazione.
	 */
	private void sheperdAnimation(final int myIndex, Coordinates xy, int sheperdIndex) {

		for(Coordinates c : roads.keySet()){
			if(xy.equals(c)){
				roads.get(c).setEnabled(false);
				roads.get(c).setVisible(false);
			}
		}

		Sheperd sheperd = null;

		for (Sheperd s : sheperds){
			if(s.getPlayerIndex() == myIndex && s.getSheperdIndex() == sheperdIndex){
				sheperd = s;
			}
		}

		//Rendo visibile la label del pastore.
		sheperd.setVisible(true);

		//Avvio l'animazione della pedina pastore.
		labelAnimationThread(sheperd.getX(), sheperd.getY(), xy.getX(), xy.getY(), sheperd, false);	
	}

	/**
	 * Thread per il movimento di una label.
	 * @param x1 Coordinata x di partenza.
	 * @param y1 Coordinata y di partenza.
	 * @param x2 Coordinata x di arrivo.
	 * @param y2 Coordinata y di arrivo.
	 * @param labelToMove Label che si vuole spostare.
	 * @param isBlack
	 */
	private void labelAnimationThread(final double x1, final double y1, final double x2, final double y2, final JLabel labelToMove, final boolean isBlack) {
		Thread t = new Thread(new Runnable(){

			@Override
			public void run() {
				double x = x1, y = y1, coeffX = (x2-x1)/100, coeffY = (y2-y1)/100;		
				final long time = (long)Math.sqrt(Math.hypot(x2-x1, y2-y1));

				//Codice che gestisce la label della pecora bianca nella regione, facendola sparire o decrementando il numero di pecore presenti
				JLabel l = searchWhiteSheep(new Coordinates((int)x1, (int)y1));
				if(l != null && !isBlack){
					if("".equals(l.getText())){
						l.setVisible(false);
					}else{
						l.setText(""+(Integer.parseInt(l.getText())-1));
						if("1".equals(l.getText())){
							l.setText("");
						}
					}
				}

				for(int i=0; i<100; i++){
					x = x + coeffX;
					y = y + coeffY;
					labelToMove.setBounds((int)x, (int)y, 26, 37);
					labelToMove.setVisible(true);
					try {
						Thread.sleep(time);
					} catch (InterruptedException e) {
						Main.LOGGER.log(Level.SEVERE, "Errore thread animazione: " + e.getMessage());
					}
				}

				//Codice che gestisce la label della pecora bianca nella regione, facendola ricomparire o incrementando il numero di pecore presenti
				l = searchWhiteSheep(new Coordinates((int)x2, (int)y2));
				if(l != null && !isBlack){
					if(!(l.isVisible())){
						l.setText("");
					}else{
						if("".equals(l.getText())){
							l.setText("2");
						}else{
							l.setText("" + (Integer.parseInt(l.getText())+1));
						}
					}
					l.setVisible(true);
				}
				whiteSheepToMove.setVisible(false);
			}
		});
		t.start();
	}

	/**
	 * Procedura che aggiorna a video il valore del dado.
	 * @param diceResult valore del lancio del dado da visualizzare.
	 */
	private void updateDice(int diceResult) {
		dice.setIcon(new ImageIcon(this.getClass().getResource("/img/dice/"+diceResult+".png")));
	}

	/** 
	 * Piazzamento a video dei recinti, normali o finali sulla mappa.
	 * @param xy coordinate della strada su cui piazzare il recinto.
	 * @param finalFence variabile booleana che indica se si deve utilizzare i recinti finali.
	 */
	private void placeFence(Coordinates xy, boolean finalFence){

		JLabel fence;
		if(!(finalFence)){
			fence = new JLabel(new ImageIcon(this.getClass().getResource("/img/fences.png")));
			fencesNumber.setText("" + ((Integer.parseInt(fencesNumber.getText()))-1));
			if("9".equals(fencesNumber.getText())){
				fencesNumber.setIconTextGap(-50);
			}
			if("0".equals(fencesNumber.getText())){
				fencesNumber.setText("");
				fencesNumber.setIcon(new ImageIcon(this.getClass().getResource("/img/FinalFence.png")));
			}
		}else{
			fence = new JLabel(new ImageIcon(this.getClass().getResource("/img/final_fences.png")));
		}

		fence.setBounds(xy.getX(), xy.getY(), 25, 25);
		fence.setVisible(true);
		mapPanel.add(fence);
	}

	/**
	 * Procedura che ricerca la label della pecora bianca associata alle coordinate in ingresso.
	 * @param xy coordinate relative alla label della pecora bianca.
	 * @return la label della pecora bianca associata alle coordinate.
	 */
	private JLabel searchWhiteSheep(Coordinates xy){
		for(JLabel sheep : whiteSheep){
			if(xy.getX() == sheep.getX() && xy.getY() == sheep.getY()){
				return sheep;
			}
		}
		return null;
	}

	/**
	 * Procedura che aggiorna il costo delle tessere terreno a video.
	 * @param cardsCurrentCost hashmap che contiene l'attuale costo delle tessere terreno.
	 */
	private void updateCardsCurrentCost(Map<LandType, Integer> cardsCurrentCost){
		for(Entry<LandType, JButton> x : cards.entrySet()){
			for(Entry<LandType, Integer> y : cardsCurrentCost.entrySet()){
				if(x.getKey().equals(y.getKey())){
					if(y.getValue()<5){
						x.getValue().setText(""+y.getValue());
						x.getValue().setBorder(null);
						x.getValue().setContentAreaFilled(false);
						x.getValue().setFont(new Font(null, Font.BOLD, 40));
					}else{
						x.getValue().setText("");
						x.getValue().setBorder(null);
						x.getValue().setContentAreaFilled(false);
						x.getValue().setEnabled(false);
					}
				}
			}
		}
	}

	@Override
	public void run() {
		gameScreen.setVisible(true);
	}

	@Override
	public void update(Observable o, Object arg) {
		switch(((GenericEvent)arg).getType()){

		case PLAYERS_NAMES_SETTED_EVENT:
			gameScreen.setVisible(true);
			InitialPhaseEvent event = new InitialPhaseEvent();
			setChanged();
			notifyObservers(event);
			break;

		case PLAYERS_READY_EVENT:
			gameScreen.setVisible(true);
			break;

		case ADD_ROAD_EVENT:
			drawRoadButton(((AddRoadEvent)arg).getRoad());
			break;

		case ADD_REGION_EVENT:
			drawAnimals(((AddRegionEvent)arg).getRegion());
			break;

		case INITIAL_COSTS_SETTED_EVENT: 
			drawGameCards();
			break;

		case CARDS_CHANGED_EVENT:
			updateCardsOwned(((CardsChangedEvent)arg).getCardsOwned());
			break;

		case PLAYER_CHANGED_EVENT:
			changePlayerInfo(((PlayerChangedEvent)arg).getPlayer());
			break;

		case UPDATE_INSTRUCTION_EVENT:
			updateInstruction(((UpdateInstructionEvent)arg).getInstruction(), ((UpdateInstructionEvent)arg).getPlayerIndex());
			break;

		case ASK_WHICH_SHEEP_EVENT:
			enableRegions(false);
			chooseWhichSheepPanel.setVisible(true);
			moveWhite.setVisible(true);
			moveBlack.setVisible(true);
			break;

		case MOVE_BLACK_SHEEP_EVENT:
			labelAnimationThread(blackSheep.getX(), blackSheep.getY(), ((MoveBlackSheepEvent)arg).getXY().getX()-18, ((MoveBlackSheepEvent)arg).getXY().getY()-18,blackSheep, true);
			break;

		case ENABLE_MOVE_SHEEP_EVENT:
			moveSheep.setEnabled(((EnableMoveSheepEvent)arg).isEnable());
			break;

		case MOVE_WHITE_SHEEP_EVENT:
			whiteSheepToMove.setVisible(true);
			labelAnimationThread(((MoveWhiteSheepEvent)arg).getSource().getX(), ((MoveWhiteSheepEvent)arg).getSource().getY(), ((MoveWhiteSheepEvent)arg).getDestination().getX(), ((MoveWhiteSheepEvent)arg).getDestination().getY(), whiteSheepToMove, false);
			break;

		case ENABLE_BUY_CARD_EVENT:
			buyCard.setEnabled(((EnableBuyCardEvent)arg).isEnable());
			break;

		case UPDATE_CARDS_CURRENT_COST_EVENT:
			updateCardsCurrentCost(((UpdateCardsCurrentCostEvent)arg).getCardsCurrentCost());
			break;

		case UPDATE_MONEY_EVENT:
			updateMoney(((UpdateMoneyEvent)arg).getMoney());
			break;

		case OCCUPIED_ROAD_EVENT:
			sheperdAnimation(((OccupiedRoadEvent)arg).getPlayerIndex(), ((OccupiedRoadEvent)arg).getXY(), ((OccupiedRoadEvent)arg).getSheperdIndex());
			break;

		case UPDATE_DICE_RESULT_EVENT:
			updateDice(((UpdateDiceResultEvent)arg).getDiceResult());
			break;

		case PLACE_FENCES_EVENT:
			placeFence(((PlaceFencesEvent)arg).getXY(), ((PlaceFencesEvent)arg).isFinalFence());
			break;

		case FINAL_SCREEN_EVENT:
			new Thread(new Runnable(){
				@Override
				public void run() {
					gameScreen.dispose();	
				}	
			}).start();
			new Thread(new FinalView(((FinalScreenEvent)arg).getPlayers())).start();
			break;

		default:
			break;
		}
	}

	/**
	 * Classe MapListener che implementa MouseListener usata per captare gli input
	 * dell'utente sulla mappa di gioco.
	 * @author Paolo Guglielmino
	 * @author Simone Martinelli
	 */
	public class MapListener implements MouseListener{

		//Se faccio click con il mouse sull'area.
		@Override
		public void mouseClicked(MouseEvent e) {
			int x = e.getX();
			int y = e.getY();
			Color c = new Color(greyMapBuffered.getRGB(x, y));

			ChosedRegionEvent event = new ChosedRegionEvent(c.getRed());
			setChanged();
			notifyObservers(event);
		}

		@Override
		public void mousePressed(MouseEvent e) {
		}

		@Override
		public void mouseReleased(MouseEvent e) {
		}

		@Override
		public void mouseEntered(MouseEvent e) {
		}

		@Override
		public void mouseExited(MouseEvent e) {
		}
	}
}