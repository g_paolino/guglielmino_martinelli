package it.polimi.guglielmino_martinelli.view;

import it.polimi.guglielmino_martinelli.WrongNumberException;
import it.polimi.guglielmino_martinelli.events.GenericEvent;
import it.polimi.guglielmino_martinelli.events.PlayersNamesInsertedEvent;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Scanner;

/**
 * Interfaccia grafica testuale, realizzata solo a scopo dimostrativo, incompleta.
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public class ConsoleView extends View{

	private Scanner scanner;
	private PrintStream output;

	public ConsoleView(InputStream inputStream, OutputStream output) {
		this.scanner = new Scanner(inputStream);
		this.output = new PrintStream(output);
	}

	/**
	 * Procedura per la raccolta del numero e dei nomi dei giocatori.
	 */
	private void askNumberOfPlayers(){
		String input;
		int numberOfPlayers = 0;
		Boolean notAnInteger = true;
		List<String> names = new ArrayList<String>();

		do{
			output.println("Inserisci il numero di giocatori: ");
			input = scanner.next();

			try{
				numberOfPlayers = Integer.parseInt(input);
				if(numberOfPlayers < 2 || numberOfPlayers > 4){
					throw new WrongNumberException();
				}else{
					notAnInteger = false;
				}
			}catch(NumberFormatException | WrongNumberException e){
				output.println("Si prega di inserire un numero intero tra 2 e 4.");
				notAnInteger = true;
			}
		}while(notAnInteger);

		for(int i = 1; i<= numberOfPlayers; i++){
			output.println("Giocatore "+i+" inserisci il tuo nome: ");
			names.add(scanner.next());
		}

		PlayersNamesInsertedEvent event = new PlayersNamesInsertedEvent(names);
		setChanged();
		notifyObservers(event);
	}

	@Override
	public void run() {
		askNumberOfPlayers();
	}

	//Switch incompleto, console view realizzata solo a scopo dimostrativo.
	@Override
	public void update(Observable o, Object arg) {
		switch(((GenericEvent)arg).getType()){

		//I nomi dei giocatori sono stati settati nel model.
		case PLAYERS_NAMES_SETTED_EVENT:
			output.println("Giocatori inseriti correttamente");
			break;

		default:
			break;
		}
	}
}