package it.polimi.guglielmino_martinelli.view;

import it.polimi.guglielmino_martinelli.events.GenericEvent;

import java.util.Observable;
import java.util.Observer;

/**
 * Classe astratta view, è Observable e implementa Observer.
 * @author Paolo Guglielmino
 * @authore Simone Martinelli
 */
public abstract class View extends Observable implements Runnable, Observer {

	public int getNumberOfPlayers() {
		return 0;
	}

	public String getPlayerName(int clientIndex) {
		return null;
	}

	public int getPlayerMoney(int clientIndex) {
		return 0;
	}

	public void sendEvent(GenericEvent event) {
	}
}