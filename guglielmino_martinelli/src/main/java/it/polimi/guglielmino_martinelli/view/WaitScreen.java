package it.polimi.guglielmino_martinelli.view;

import java.awt.Color;
import java.awt.Font;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

/**
 * Schermata di attesa per l'arrivo di altri giocatori in rete.
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public class WaitScreen implements Runnable{
	/**
	 * Finestra per l'attesa dei giocatori.
	 */
	private JFrame waitScreenFrame;
	
	/**
	 * Costruttore utilizzato semplicemente per creare la schermata di attesa.
	 * @param ip indirizzo del server.
	 * @param port porta del server.
	 */
	public WaitScreen(String ip, int port){

		waitScreenFrame = new JFrame("Attesa giocatori...");
		waitScreenFrame.setSize(300, 248);
		waitScreenFrame.setResizable(false);
		waitScreenFrame.setLocationRelativeTo(null);
		waitScreenFrame.setLayout(null);
		waitScreenFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		BackgroundPanel bg = new BackgroundPanel(this.getClass().getResource("/img/wait_screen.png"));
		bg.setLayout(null);
		waitScreenFrame.add(bg);

		ImageIcon img = new ImageIcon(this.getClass().getResource("/img/waiting.gif"));
		JLabel sheepWaiting = new JLabel(img);
		sheepWaiting.setBounds(171, 102, 40, 40);

		JLabel message = new JLabel("Attesa giocatori su:");
		message.setBounds(0, 20, 300, 20);
		message.setHorizontalAlignment(SwingConstants.CENTER);
		message.setFont(new Font(null, Font.BOLD, 13));
		message.setForeground(new Color(70,70,70));

		JLabel serverData = new JLabel(ip + ":" + port);
		serverData.setBounds(0, 40, 300, 20);
		serverData.setHorizontalAlignment(SwingConstants.CENTER);
		serverData.setFont(new Font(null, Font.BOLD, 13));

		bg.add(message);
		bg.add(serverData);
		bg.add(sheepWaiting);
	}

	/**
	 * Costruttore che richiama quello precedente, e in più realizza la chiusura della scermata
	 * dopo un certo intervallo di tempo.
	 * @param ip indirizzo del server.
	 * @param port porta del server.
	 * @param time tempo di esposizione della schermata.
	 */
	public WaitScreen(String ip, int port, int time){
		this(ip, port);
		Timer timer = new Timer();
		timer.schedule(new TimerTask(){
			@Override
			public void run() {
				waitScreenFrame.dispose();
			}
		}, time);
	}

	public JFrame getWaitScreenFrame() {
		return waitScreenFrame;
	}

	public void waitEnded(){
		waitScreenFrame.dispose();
	}

	@Override
	public void run() {
		waitScreenFrame.setVisible(true);
	}
}
