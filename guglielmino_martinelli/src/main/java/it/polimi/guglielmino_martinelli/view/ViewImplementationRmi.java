package it.polimi.guglielmino_martinelli.view;

import it.polimi.guglielmino_martinelli.Main;
import it.polimi.guglielmino_martinelli.controller.GameManager;
import it.polimi.guglielmino_martinelli.events.GenericEvent;
import it.polimi.guglielmino_martinelli.model.GameStatus;
import it.polimi.guglielmino_martinelli.rmi.ServerImplementation;

import java.rmi.RemoteException;
import java.util.Observable;
import java.util.logging.Level;

public class ViewImplementationRmi extends View{
	
	/**
	 * Istanza del controller del gioco.
	 */
	private GameManager manager;
	/**
	 * Istanza del model del gioco.
	 */
	private GameStatus status;
	/**
	 * Numero di giocatori che hanno completato la fase di inserimento dei dati e sono pronti al gioco.
	 */
	private int playersReady;
	
	private ServerImplementation server;
	
	public ViewImplementationRmi(int numberOfPlayers){
		this.playersReady = 0;
		this.status = new GameStatus();
		this.manager = new GameManager(status, this);
		status.addObserver(this);
		status.setNumberOfPlayers(numberOfPlayers);
	}

	public void setServer(ServerImplementation server) {
		this.server = server;
	}

	@Override
	public int getNumberOfPlayers() {
		return status.getNumberOfPlayers();
	}
	
	@Override
	public String getPlayerName(int clientIndex) {
		return status.getPlayers().get(clientIndex).getName();
	}

	@Override
	public int getPlayerMoney(int clientIndex) {
		return status.getPlayers().get(clientIndex).getMoney();
	}

	/**
	 * Passaggio dell'evento al Controller.
	 */
	@Override
	public void sendEvent(GenericEvent event) {
		manager.update(null, event);
	}

	@Override
	public void run() {
	}

	@Override
	public void update(Observable o, Object arg) {
		
		switch(((GenericEvent)arg).getType()){
		
		//E' stato appena settato il nome di un giocatore nel model.
		case PLAYER_NAME_SETTED_EVENT: 
			//Aumento di 1 il numero di giocatori pronti.
			playersReady++;
			//Se i giocatori sono tutti pronti.
			if(playersReady == status.getNumberOfPlayers()){
				try {
					//Dice ai client di visualizzare la finestra di gioco.
					server.goClients();
					
					//Faccio partire le operazioni iniziali del gioco.
					manager.initialPhase();
				} catch (RemoteException e) {
					Main.LOGGER.log(Level.SEVERE, "Il metodo remoto non è raggiungibile (showGameView())", e);
				}
			}
			break;
			
		//Le tessere possedute da un giocatore sono cambiate.
		case CARDS_CHANGED_EVENT:
		//Bisogna aggiornare le istruzioni per un certo client.
		case UPDATE_INSTRUCTION_EVENT:
		//E' scattato il cambio di giocatore.
		case PLAYER_CHANGED_EVENT:
		//E' stato cambiato il valore dei soldi.
		case UPDATE_MONEY_EVENT:
		//Devo chiedere al giocatore se vuole muovere la pecora bianca o la nera.
		case ASK_WHICH_SHEEP_EVENT:
		//La mossa "Muovi pecora" non è più/è di nuovo disponibile.
		case ENABLE_MOVE_SHEEP_EVENT:
		//La mossa "Compra tessera" non è più/è di nuovo disponibile.
		case ENABLE_BUY_CARD_EVENT:
			try {
				server.sendEventToClient((GenericEvent)arg, status.getCurrentPlayerIndex());
			} catch (RemoteException e) {
				Main.LOGGER.log(Level.SEVERE, "Remote Exception", e);
			}
			break;
			
		default:
			try {
				server.sendBroadcastEvent((GenericEvent) arg);
			} catch (RemoteException e) {
				Main.LOGGER.log(Level.SEVERE, "Remote Exception", e);
			}
			break;
		}
	}
}