package it.polimi.guglielmino_martinelli.view;

import it.polimi.guglielmino_martinelli.Main;
import it.polimi.guglielmino_martinelli.rmi.MainClient;
import it.polimi.guglielmino_martinelli.socket.MyClientSocket;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.text.PlainDocument;

/**
 * Finestra per l'inserimento dei dati del giocatore.
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public class PlayerInfoView implements Runnable{
	
	/**
	 * Finestra principale.
	 */
	private JFrame playerInfoScreen;
	/**
	 * TextField per il nome del giocatore.
	 */
	private JTextField name;
	/**
	 * Bottono per inviare i dati inseriti.
	 */
	private JButton submitName;
	/**
	 * Variabili booleane per eseguire il controllo sulla possibilita di cliccare il bottone.
	 */
	private boolean nameInserted;
	private boolean ipInserted;
	
	public PlayerInfoView(final boolean networkFlag){
		
		//Creo la finestra playerInfoScreen (JFrame).
		playerInfoScreen = new JFrame("Sheepland");
		playerInfoScreen.setSize(900, 700);
		playerInfoScreen.setResizable(false);
		playerInfoScreen.setLocationRelativeTo(null);
		playerInfoScreen.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		//Creo lo playerInfoMainPanel con lo sfondo.
		JPanel playerInfoMainPanel = new BackgroundPanel(this.getClass().getResource("/img/SHEEPLAND_home.png"));
		playerInfoMainPanel.setLayout(null);
		
		playerInfoScreen.add(playerInfoMainPanel);
		
		
		//Istruzioni al giocatore.
		Color c = new Color(121,58,22);
		JLabel instruction = new JLabel("Come ti chiami?");
		instruction.setBounds(0, 300, 900, 20);
		instruction.setHorizontalAlignment(SwingConstants.CENTER);
		instruction.setFont(new Font(null, Font.BOLD, 15));
		instruction.setForeground(c);
		playerInfoMainPanel.add(instruction);
		
		JLabel serverAddress = new JLabel("Server:");
		serverAddress.setBounds(325, 215, 75, 35);
		serverAddress.setFont(new Font(null, Font.BOLD, 15));
		serverAddress.setForeground(c);
		playerInfoMainPanel.add(serverAddress);
		
		//TextField per inserimento dati di connessione e nome del giocatore.
		name = new JTextField();
		this.nameInserted = false;
		name.setBounds(375, 325, 150, 35);
		name.setVisible(true);
		
		playerInfoMainPanel.add(name);
		
		final JTextField ipTf = new JTextField("localhost");
		this.ipInserted = true;
		ipTf.setBounds(390, 215, 135, 35);
		
		final JTextField portTf = new JTextField();
		
		//Controllo, stabilisco di default due numeri di porta di versi per rmi e socket.
		if(networkFlag){
			portTf.setText("8444");
		}else{
			portTf.setText("8445");
		}
		
		portTf.setBounds(530, 215, 50, 35);
		
		playerInfoMainPanel.add(ipTf);
		playerInfoMainPanel.add(portTf);
		
		//Filtro per la Text Field con il numero di porta, accetta solo numeri interi.
		PlainDocument doc = (PlainDocument) portTf.getDocument();
	    doc.setDocumentFilter(new MyIntFilter());
		
		//Creo il bottone per la conferma.
		ImageIcon arrow = new ImageIcon(this.getClass().getResource("/img/arrow.png"));
		submitName = new JButton(arrow);
		submitName.setBounds(530, 400, arrow.getIconWidth(), arrow.getIconHeight());
		submitName.setBorder(null);
		submitName.setContentAreaFilled(false);
		submitName.setEnabled(false);
		
		playerInfoMainPanel.add(submitName);
		
		
		ipTf.addCaretListener(new CaretListener(){

			@Override
			public void caretUpdate(CaretEvent e) {
				if("".equals(ipTf.getText())){
					ipInserted = false;
				}else{
					ipInserted = true;
				}
				checkSubmitButton();
			}
			
		});
		//Aggiungo il listener della JTextField name (controllo che non sia vuota).
		name.addCaretListener(new CaretListener(){

			@Override
			public void caretUpdate(CaretEvent e) {
				if ("".equals(name.getText())){
					nameInserted = false;
				} else {
					nameInserted = true;
				}
				checkSubmitButton();
			}
			
		});
		
		//Aggiungo il listener del bottone submitNames (Freccia di legno "Vai").
		submitName.addActionListener(new ActionListener(){
			
			@Override
			public void actionPerformed(ActionEvent e){
				
				if(networkFlag){
					new MainClient(ipTf.getText(), Integer.parseInt(portTf.getText()), name.getText()).avviaClient();
				}else{
					try {
						new MyClientSocket(name.getText(), ipTf.getText(), Integer.parseInt(portTf.getText()));
					} catch (NumberFormatException e1) {
						Main.LOGGER.log(Level.SEVERE, "Errore", e1.getMessage());
					}
				}
				
				playerInfoScreen.dispose();
			}
		});
		
	}
	
	private void checkSubmitButton(){
		submitName.setEnabled(nameInserted && ipInserted);
	}
	
	@Override
	public void run() {
		playerInfoScreen.setVisible(true);
	}
}
