package it.polimi.guglielmino_martinelli.view;

import it.polimi.guglielmino_martinelli.model.LandType;

import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.ImageIcon;
import javax.swing.JButton;

public class Card extends JButton{
	
	private static final long serialVersionUID = 6030862659769602733L;
	/**
	 * Immagine di sfondo di questa tessera.
	 */
	private ImageIcon img;
	/**
	 * Tipo di terreno di questa tessera.
	 */
	private LandType landType;
	
	public Card(String img, LandType landType){
		this(new ImageIcon(img));
		this.landType = landType;
	}
	
	public Card(ImageIcon img) {
		this.img = img;
		Dimension size = new Dimension(img.getIconWidth(), img.getIconHeight());
		setPreferredSize(size);
		setMinimumSize(size);
		setMaximumSize(size);
		setSize(size);
		setLayout(null);
		this.setDisabledIcon(img);
	}
	
	public void paintComponent(Graphics g) {
		g.drawImage(img.getImage(), 0, 0, null);
	}

	public LandType getLandType() {
		return landType;
	}	
}