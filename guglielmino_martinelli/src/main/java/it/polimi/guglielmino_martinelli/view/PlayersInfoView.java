package it.polimi.guglielmino_martinelli.view;

import it.polimi.guglielmino_martinelli.events.PlayersNamesInsertedEvent;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

/**
 * Scermata di inserimento del numero e nomi dei giocatori, nel caso di gioco in locale.
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public class PlayersInfoView extends View {
	
	private JFrame playersInfoViewFrame;
	private JComboBox <String> comboPlayers;
	private JTextField tf1;
	private JTextField tf2;
	private JTextField tf3;
	private JTextField tf4;
	private JLabel player3;
	private JLabel player4;
	private JButton submitName;
	private boolean name1;
	private boolean name2;
	private boolean name3;
	private boolean name4;
	
	public PlayersInfoView() {
			    
		//ArrayList per i nomi dei giocatori.
		final List<String> names = new ArrayList<String>();
		
		//Finestra principale.
		playersInfoViewFrame = new JFrame("Sheepland");
		playersInfoViewFrame.setSize(900, 700);
		playersInfoViewFrame.setResizable(false);
		playersInfoViewFrame.setLocationRelativeTo(null);
		playersInfoViewFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel playersInfoMainPanel = new BackgroundPanel(this.getClass().getResource("/img/SHEEPLAND_home.png"));
		playersInfoMainPanel.setLayout(null);
		playersInfoViewFrame.add(playersInfoMainPanel);
		
		//Label numero giocatori.
		Color c = new Color(121,58,22);
		JLabel numOfPlayers = new JLabel("# Giocatori");
		numOfPlayers.setBounds(245, 200, 150, 35);
		numOfPlayers.setFont(new Font(null, Font.BOLD, 15));
		numOfPlayers.setForeground(c);
		playersInfoMainPanel.add(numOfPlayers);
		
		//ComboBox per la scelta del numero di giocatori.
		String[] s = {"2","3","4"};
		comboPlayers = new JComboBox<String>(s);
		comboPlayers.setBounds(260, 235, 60, 30);
		comboPlayers.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				if(Integer.parseInt((String)comboPlayers.getSelectedItem()) > 2){
					tf3.setVisible(true);
					tf4.setVisible(false);
					tf4.setText("");
					
					if("4".equals(comboPlayers.getSelectedItem())){
						tf4.setVisible(true);
					}
				}else{
					tf3.setVisible(false);
					tf3.setText("");
					tf4.setVisible(false);
					tf4.setText("");
				}
				checkSubmitButton();
				playersInfoViewFrame.repaint();
			}
		});
		
		//TextField per l'inserimento dei nomi dei giocatori.
		tf1 = new JTextField();
		tf2 = new JTextField();
		tf3 = new JTextField();
		tf4 = new JTextField();
		tf1.setBounds(520, 200, 150, 35);
		tf2.setBounds(520, 250, 150, 35);
		tf3.setBounds(520, 300, 150, 35);
		tf4.setBounds(520, 350, 150, 35);
		tf3.setVisible(false);
		tf4.setVisible(false);
		
		tf1.addCaretListener(new CaretListener(){
			@Override
			public void caretUpdate(CaretEvent e) {
				if ("".equals(tf1.getText())){
					name1 = false;
				} else {
					name1 = true;
				}
				checkSubmitButton();
			}
		});
		
		tf2.addCaretListener(new CaretListener(){
			@Override
			public void caretUpdate(CaretEvent e) {
				if ("".equals(tf2.getText())){
					name2 = false;
				} else {
					name2 = true;
				}
				checkSubmitButton();
			}
		});
		
		tf3.addCaretListener(new CaretListener(){
			@Override
			public void caretUpdate(CaretEvent e) {
				if ("".equals(tf3.getText())){
					name3 = false;
				} else {
					name3 = true;
				}
				checkSubmitButton();
			}
		});
		
		tf4.addCaretListener(new CaretListener(){
			@Override
			public void caretUpdate(CaretEvent e) {
				if ("".equals(tf4.getText())){
					name4 = false;
				} else {
					name4 = true;
				}
				checkSubmitButton();
			}
		});
		
		//Label con la scritta giocatore i-esimo.
		JLabel player1 = new JLabel("Giocatore 1:");
		player1.setBounds(400, 200, 150, 30);
		player1.setFont(new Font(null, Font.BOLD, 15));
		player1.setForeground(c);
		playersInfoMainPanel.add(player1);
		
		JLabel player2 = new JLabel("Giocatore 2:");
		player2.setBounds(400, 250, 150, 30);
		player2.setFont(new Font(null, Font.BOLD, 15));
		player2.setForeground(c);
		playersInfoMainPanel.add(player2);
		
		player3 = new JLabel("Giocatore 3:");
		player3.setBounds(400, 300, 150, 30);
		player3.setFont(new Font(null, Font.BOLD, 15));
		player3.setForeground(c);
		player3.setVisible(false);
		playersInfoMainPanel.add(player3);
		
		player4 = new JLabel("Giocatore 4:");
		player4.setBounds(400, 350, 150, 30);
		player4.setFont(new Font(null, Font.BOLD, 15));
		player4.setForeground(c);
		player4.setVisible(false);
		playersInfoMainPanel.add(player4);
		
		//Creo il bottone per la conferma.
		ImageIcon arrow = new ImageIcon(this.getClass().getResource("/img/arrow.png"));
		submitName = new JButton(arrow);
		submitName.setBounds(530, 400, arrow.getIconWidth(), arrow.getIconHeight());
		submitName.setBorder(null);
		submitName.setContentAreaFilled(false);
		submitName.setEnabled(false);
		submitName.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				names.add(tf1.getText());
				names.add(tf2.getText());
				if(Integer.parseInt((String)comboPlayers.getSelectedItem()) > 2){
					names.add(tf3.getText());
					
					if("4".equals(comboPlayers.getSelectedItem())){
						names.add(tf4.getText());
					}
				}
				playersInfoViewFrame.dispose();
				PlayersNamesInsertedEvent event = new PlayersNamesInsertedEvent(names);
				setChanged();
				notifyObservers(event);
			}
		});
		
		//Aggiunta effettiva delle componeneti al pannello.
		playersInfoMainPanel.add(comboPlayers);
		playersInfoMainPanel.add(tf1);
		playersInfoMainPanel.add(tf2);
		playersInfoMainPanel.add(tf3);
		playersInfoMainPanel.add(tf4);
		playersInfoMainPanel.add(submitName);
	}
	
	/**
	 * Metdo per il controllo dell'abilitazione o meno del bottone di invio.
	 */
	private void checkSubmitButton(){
		switch((String)comboPlayers.getSelectedItem()){
		case "2":
			submitName.setEnabled(name1 && name2);
			player3.setVisible(false);
			player4.setVisible(false);
			break;
		case "3":
			submitName.setEnabled(name1 && name2 && name3);
			player3.setVisible(true);
			player4.setVisible(false);
			break;
		case "4":
			submitName.setEnabled(name1 && name2 && name3 && name4);
			player3.setVisible(true);
			player4.setVisible(true);
			break;
		default:
			break;
		}
	}

	@Override
	public void run() {
		playersInfoViewFrame.setVisible(true);
	}
	
	@Override
	public void update(Observable o, Object arg) {
	}
}