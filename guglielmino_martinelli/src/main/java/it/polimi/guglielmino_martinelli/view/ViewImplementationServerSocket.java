package it.polimi.guglielmino_martinelli.view;

import it.polimi.guglielmino_martinelli.Main;
import it.polimi.guglielmino_martinelli.controller.GameManager;
import it.polimi.guglielmino_martinelli.events.GenericEvent;
import it.polimi.guglielmino_martinelli.events.PlayersReadyEvent;
import it.polimi.guglielmino_martinelli.model.GameStatus;
import it.polimi.guglielmino_martinelli.socket.MyServerSocket;

import java.io.IOException;
import java.util.Observable;
import java.util.logging.Level;

/**
 * View usata dal server socket per la comunicazione con i client.
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public class ViewImplementationServerSocket extends View {

	private GameManager manager;
	private GameStatus status;
	private int playersReady;
	private MyServerSocket server;

	public ViewImplementationServerSocket(int numberOfPlayers){
		this.playersReady = 0;
		this.status = new GameStatus();
		this.manager = new GameManager(status, this);
		status.addObserver(this);
		status.setNumberOfPlayers(numberOfPlayers);
	}

	public void setServer(MyServerSocket server) {
		this.server = server;
	}

	/**
	 * Inoltro degli eventi al controller di gioco.
	 */
	@Override
	public void sendEvent(GenericEvent event){
		manager.update(null, event);
	}

	@Override
	public void run() {
	}

	/**
	 * Intercettamento degli eventi provenienti dal model.
	 */
	@Override
	public void update(Observable o, Object arg) {

		switch(((GenericEvent)arg).getType()){

		//E' stato appena settato il nome di un giocatore nel model.
		case PLAYER_NAME_SETTED_EVENT: 
			//Aumento di 1 il numero di giocatori pronti.
			playersReady++;
			//Se i giocatori sono tutti pronti.
			if(playersReady == status.getNumberOfPlayers()){

				PlayersReadyEvent event = new PlayersReadyEvent();

				//Dice ai client di visualizzare la finestra di gioco.
				try {
					server.sendBroadcastEvent((GenericEvent) event);
				} catch (IOException e) {
					Main.LOGGER.log(Level.SEVERE, "Errore", e.getMessage());
				}

				//Faccio partire le operazioni iniziali del gioco.
				manager.initialPhase();
			}
			break;

		//Le tessere possedute da un giocatore sono cambiate.
		case CARDS_CHANGED_EVENT:
		//Bisogna aggiornare le istruzioni per un certo client.
		case UPDATE_INSTRUCTION_EVENT:
		//E' scattato il cambio di giocatore.
		case PLAYER_CHANGED_EVENT:
		//E' stato cambiato il valore dei soldi.
		case UPDATE_MONEY_EVENT:
		//Bisogna chiedere all'utente quale pecora intende muovere.
		case ASK_WHICH_SHEEP_EVENT:
		//La mossa "Muovi pecora" non è più/è di nuovo disponibile.
		case ENABLE_MOVE_SHEEP_EVENT:
		//La mossa "Compra tessera" non è più/è di nuovo disponibile.
		case ENABLE_BUY_CARD_EVENT:
			try {
				server.sendEventToClient((GenericEvent)arg, status.getCurrentPlayerIndex());
			} catch (IOException e) {
				Main.LOGGER.log(Level.SEVERE, "Errore", e.getMessage());
			}
			break;

		default:
			try {
				server.sendBroadcastEvent((GenericEvent) arg);
			} catch (IOException e) {
				Main.LOGGER.log(Level.SEVERE, "Errore", e.getMessage());
			}
			break;
		}
	}
}