package it.polimi.guglielmino_martinelli.view;

import it.polimi.guglielmino_martinelli.controller.GameManager;
import it.polimi.guglielmino_martinelli.model.GameStatus;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;

/**
 * Finestra per la scelta della modalità di gioco.
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public class ModeView implements Runnable{
	
	/**
	 * Finestra principale.
	 */
	private JFrame modeScreen;
	/**
	 * Flag che indica la modalità di connessione che si intende usare, true = rmi e false=socket.
	 */
	private boolean networkFlag = true;
	
	public ModeView(){
		
		//Creo la finestra modeScreen (JFrame).
		modeScreen = new JFrame("Sheepland");
		modeScreen.setSize(900, 700);
		modeScreen.setResizable(false);
		modeScreen.setLocationRelativeTo(null);
		modeScreen.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		//Creo lo modeMainPanel con lo sfondo.
		JPanel modeMainPanel = new BackgroundPanel(getClass().getResource("/img/SHEEPLAND_home.png"));
		modeMainPanel.setLayout(null);
		
		modeScreen.add(modeMainPanel);
		
		
		//Scelta della modalità di gioco.
		
		JLabel instruction = new JLabel("In che modalità vuoi giocare?");
		instruction.setBounds(0, 200, 900, 20);
		instruction.setHorizontalAlignment(SwingConstants.CENTER);
		instruction.setFont(new Font(null, Font.BOLD, 15));
		Color c = new Color(121,58,22);
		instruction.setForeground(c);
		
		modeMainPanel.add(instruction);
		
		//Creo i bottoni per scegliere la modalità di gioco.
		ImageIcon localImg = new ImageIcon(this.getClass().getResource("/img/locale.png"));
		JButton local = new JButton(localImg);
		local.setBorder(null);
		local.setContentAreaFilled(false);
		local.setBounds(200, 300, localImg.getIconWidth(), localImg.getIconHeight());
		
		ImageIcon netImg = new ImageIcon(this.getClass().getResource("/img/rete.png"));
		JButton net = new JButton(netImg);
		net.setBorder(null);
		net.setContentAreaFilled(false);
		net.setBounds(500, 300, netImg.getIconWidth(), netImg.getIconHeight());
		
		modeMainPanel.add(local);
		modeMainPanel.add(net);
		
		//Aggiungo il listener del bottone per la modalità offline.
		local.addActionListener(new ActionListener(){
			
			@Override
			public void actionPerformed(ActionEvent e){
				modeScreen.dispose();
				
				//Creo Model.
				GameStatus status = new GameStatus();
				//Creo View.
				View gameView = new GameView();
				View playersInfoView = new PlayersInfoView();
				//Creo Controller.
				GameManager manager = new GameManager(status, gameView);
				
				//Aggiungo gli Observer.
				gameView.addObserver(manager);
				playersInfoView.addObserver(manager);
				status.addObserver(gameView);
				
				//Lancio la schermata di inserimento dei nomi.
				new Thread(playersInfoView).start();
			}
		});
		
		//Aggiungo il listener del bottone (JLabel) per la modalità online.
		net.addActionListener(new ActionListener(){
			
			@Override
			public void actionPerformed(ActionEvent e){
				new Thread(new RoleView(networkFlag)).start();
				modeScreen.dispose();
			}
		});
		
		//Aggiundo due RadioButton per scegliere la modalità di connessione.
		JRadioButton rmi = new JRadioButton("RMI");
		rmi.setBounds(720, 310, 100, 30);
		rmi.setBorder(null);
		rmi.setContentAreaFilled(false);
		rmi.setSelected(true);
		JRadioButton socket = new JRadioButton("SOCKET");
		socket.setBounds(720, 350, 100, 30);
		socket.setBorder(null);
		socket.setContentAreaFilled(false);
		ButtonGroup group = new ButtonGroup();
		group.add(rmi);
		group.add(socket);
		modeMainPanel.add(rmi);
		modeMainPanel.add(socket);
		
		rmi.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				networkFlag = true;
			}
		});
		
		socket.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				networkFlag = false;
			}
		});
	}
	
	@Override
	public void run() {
		modeScreen.setVisible(true);
	}
}
