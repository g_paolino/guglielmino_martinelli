package it.polimi.guglielmino_martinelli.view;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

/**
 * Classe per la creazione del pannello iniziale con lo sfondo personalizzato.
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public class BackgroundPanel extends JPanel {

	private static final long serialVersionUID = 5683376988657007398L;
	/**
	 * Oggetto immagine che sarà lo sfondo del pannello.
	 */
	private Image img;

	/**
	 * Costruttore.
	 * @param img stringa che contiene il percorso dell'immagine.
	 */
	public BackgroundPanel(URL location) {
		this(new ImageIcon(location).getImage());
	}

	/**
	 * Costruttore.
	 * @param img oggetto immagine.
	 */
	public BackgroundPanel(Image img) {
		this.img = img;
		Dimension size = new Dimension(img.getWidth(null), img.getHeight(null));
		setPreferredSize(size);
		setMinimumSize(size);
		setMaximumSize(size);
		setSize(size);
		setLayout(null);
	}

	/**
	 * Procedura che disegna il pannello sullo schermo.
	 */
	public void paintComponent(Graphics g) {
		g.drawImage(img, 0, 0, null);
	}
}