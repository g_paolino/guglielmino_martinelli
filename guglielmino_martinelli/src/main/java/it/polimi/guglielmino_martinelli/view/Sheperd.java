package it.polimi.guglielmino_martinelli.view;

import java.awt.Dimension;
import java.awt.Graphics;
import java.io.Serializable;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 * Label personalizzata, per contenere uno sfondo, il numero del giocatore a cui appartiene ed l'indice del pastore.
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public class Sheperd extends JLabel implements Serializable {

	private static final long serialVersionUID = 3803748332084086848L;
	/**
	 * Immagine di sfondo di questo pastore.
	 */
	private ImageIcon img;
	/**
	 * Indice del giocatore a cui appartiene questo pastore.
	 */
	private int playerIndex;
	/**
	 * Indice del pastore.
	 */
	private int sheperdIndex;
	
	public Sheperd(ImageIcon img, int playerIndex, int sheperdIndex){
		this(img);
		this.playerIndex = playerIndex;
		this.sheperdIndex = sheperdIndex;
	}

	public Sheperd(ImageIcon img) {
		this.img = img;
		Dimension size = new Dimension(img.getIconWidth(), img.getIconHeight());
		setPreferredSize(size);
		setMinimumSize(size);
		setMaximumSize(size);
		setSize(size);
		setLayout(null);
		this.setDisabledIcon(img);
	}
	
	public void paintComponent(Graphics g) {
		g.drawImage(img.getImage(), 0, 0, null);
	}

	public int getPlayerIndex() {
		return playerIndex;
	}

	public void setPlayerIndex(int playerIndex) {
		this.playerIndex = playerIndex;
	}
	
	public int getSheperdIndex(){
		return sheperdIndex;
	}

	public ImageIcon getImg() {
		return img;
	}
}