package it.polimi.guglielmino_martinelli.view;

import it.polimi.guglielmino_martinelli.model.Player;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/**
 * Schermata che visualizza i punteggi a fine partita.
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public class FinalView implements Runnable{
	
	/**
	 * Finestra principale.
	 */
	private JFrame finalScreen;
	
	public FinalView(Map<Integer, Player> finalPlayers){
		Map<Integer, Player> players = finalPlayers;
		
		//Creo la finestra finalScreen (JFrame).
		finalScreen = new JFrame("Sheepland");
		finalScreen.setSize(900, 700);
		finalScreen.setResizable(false);
		finalScreen.setLocationRelativeTo(null);
		finalScreen.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		//Creo lo finalMainPanel con lo sfondo.
		JPanel finalMainPanel = new BackgroundPanel(getClass().getResource("/img/SHEEPLAND_final.png"));
		finalMainPanel.setLayout(null);
		
		finalScreen.add(finalMainPanel);
		
		//Creo la label per la gif dei coriandoli.
		JLabel coriander = new JLabel(new ImageIcon(this.getClass().getResource("/img/coriander.gif")));
		coriander.setBounds(0, -5, 900, 700);
		
		finalMainPanel.add(coriander);
		
		Color c = new Color(121,58,22);
		
		JLabel instruction = new JLabel("Punteggi finali");
		instruction.setBounds(0, 200, 900, 20);
		instruction.setHorizontalAlignment(SwingConstants.CENTER);
		instruction.setFont(new Font(null, Font.BOLD, 18));
		instruction.setForeground(c);
		
		finalMainPanel.add(instruction);
		
		Player playerTemp;
		
		for (int j=1; j<=players.size(); j++) {
			
			for (int i=players.size()-1; i>=j; i--) {
				if(players.get(i).getMoney()<players.get(i+1).getMoney()){
					playerTemp = players.get(i);
					players.put(i, players.get(i+1));
					players.put(i+1, playerTemp);
					
				}
			}
		}
		
		//Creo la lista per le pedine dei pastori.
		List <Sheperd> sheperds = new ArrayList<Sheperd>();
		sheperds.add(new Sheperd(new ImageIcon(this.getClass().getResource("/img/blue_sheperd_round.png")), 1, 1));
		sheperds.add(new Sheperd(new ImageIcon(this.getClass().getResource("/img/red_sheperd_round.png")), 2, 1));
		
		if(players.size() > 2){
			
			sheperds.add(new Sheperd(new ImageIcon(this.getClass().getResource("/img/green_sheperd_round.png")), 3, 1));
			if(players.size() == 4){
				
				sheperds.add(new Sheperd(new ImageIcon(this.getClass().getResource("/img/yellow_sheperd_round.png")), 4, 1));
			}
		}
		
		JLabel place1 = new JLabel(new ImageIcon(this.getClass().getResource("/img/1place.png")));
		place1.setBounds(245, 222, 60, 70);
		finalMainPanel.add(place1);
		
		JLabel place2 = new JLabel(new ImageIcon(this.getClass().getResource("/img/2place.png")));
		place2.setBounds(260, 290, 40, 51);
		finalMainPanel.add(place2);
		
		if(players.size() > 2){
			JLabel place3 = new JLabel(new ImageIcon(this.getClass().getResource("/img/3place.png")));
			place3.setBounds(265, 340, 35, 45);
			finalMainPanel.add(place3);
			
			if(players.size() == 4){
				JLabel place4 = new JLabel(new ImageIcon(this.getClass().getResource("/img/spoon.png")));
				place4.setBounds(267, 390, 45, 46);
				finalMainPanel.add(place4);
			}
		}
		
		JLabel p;
		int i = 1;
		//Visualizzo la classifica finale a video.
		for(Player player : players.values()){
			for(Sheperd sheperd : sheperds){
				if(player.getIndex() == sheperd.getPlayerIndex()){
					
					sheperd.setBounds(290, 200 + (i*50), 25, 25);
					finalMainPanel.add(sheperd);
					
					p = new JLabel("Giocatore: " + player.getName() + "    Danari: " + player.getMoney());
					p.setBounds(340, 200 + (i*50), 500, 25);
					p.setFont(new Font(null, Font.BOLD, 14));
					p.setForeground(c);
					finalMainPanel.add(p);
					
					i++;
				}
			}
		}
	}

	@Override
	public void run() {
		finalScreen.setVisible(true);
		
	}
}
