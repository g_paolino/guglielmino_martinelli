package it.polimi.guglielmino_martinelli.view;

import it.polimi.guglielmino_martinelli.Main;
import it.polimi.guglielmino_martinelli.rmi.MainServer;
import it.polimi.guglielmino_martinelli.socket.MyServerSocket;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.Inet4Address;
import java.util.logging.Level;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.text.PlainDocument;

/**
 * Finestra per l'inserimento del numero di giocatori ammessi.
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public class PlayersNumberView implements Runnable {

	/**
	 * Finestra principale.
	 */
	private JFrame playersNumberScreen;
	/**
	 * ComboBox per la scelta del numero di giocatori.
	 */
	private JComboBox<String> comboPlayers;
	/**
	 * TextField per il nome del giocatore.
	 */
	private JTextField name;
	/**
	 * TextField per il numero di porta.
	 */
	private JTextField port;


	public PlayersNumberView(final boolean networkFlag){

		//Creo la finestra playerNumbersScreen (JFrame).
		playersNumberScreen = new JFrame("Sheepland");
		playersNumberScreen.setSize(900, 700);
		playersNumberScreen.setResizable(false);
		playersNumberScreen.setLocationRelativeTo(null);
		playersNumberScreen.setVisible(true);
		playersNumberScreen.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		//Creo lo playerInfoMainPanel con lo sfondo.
		JPanel playersNumberMainPanel = new BackgroundPanel(this.getClass().getResource("/img/SHEEPLAND_home.png"));
		playersNumberMainPanel.setLayout(null);

		playersNumberScreen.add(playersNumberMainPanel);


		//Istruzioni al giocatore.
		Color c = new Color(121,58,22);
		JLabel instruction = new JLabel("In quanti volete giocare?");
		instruction.setBounds(0, 200, 900, 20);
		instruction.setHorizontalAlignment(SwingConstants.CENTER);
		instruction.setFont(new Font(null, Font.BOLD, 15));
		instruction.setForeground(c);
		playersNumberMainPanel.add(instruction);
		
		//ComboBox per impostare il numero di giocatori.
		String[] s = {"2","3","4"};
		comboPlayers = new JComboBox<String>(s);
		comboPlayers.setBounds(425, 230, 60, 25);

		playersNumberMainPanel.add(comboPlayers);

		JLabel instructionName = new JLabel("Come ti chiami?");
		instructionName.setBounds(0, 280, 900, 20);
		instructionName.setHorizontalAlignment(SwingConstants.CENTER);
		instructionName.setFont(new Font(null, Font.BOLD, 15));
		instructionName.setForeground(c);
		playersNumberMainPanel.add(instructionName);

		name = new JTextField();
		name.setBounds(377, 305, 150, 35);
		name.setVisible(true);

		playersNumberMainPanel.add(name);

		JLabel instructionPort = new JLabel("Scegli port:");
		instructionPort.setBounds(380, 350, 120, 35);
		instructionPort.setFont(new Font(null, Font.BOLD, 15));
		instructionPort.setForeground(c);
		playersNumberMainPanel.add(instructionPort);

		port = new JTextField();
		port.setBounds(477, 350, 50, 35);
		port.setVisible(true);

		//Filtro per la Text Field con il numero di porta, accetta solo numeri interi.
		PlainDocument doc = (PlainDocument) port.getDocument();
		doc.setDocumentFilter(new MyIntFilter());

		//Controllo, stabilisco di default due numeri di porta di versi per rmi e socket.
		if(networkFlag){
			port.setText("8444");
		}else{
			port.setText("8445");
		}

		playersNumberMainPanel.add(port);

		//Creo il bottone per la conferma.
		ImageIcon arrow = new ImageIcon(this.getClass().getResource("/img/arrow.png"));
		final JButton submitName = new JButton(arrow);
		submitName.setBounds(530, 400, arrow.getIconWidth(), arrow.getIconHeight());
		submitName.setBorder(null);
		submitName.setContentAreaFilled(false);
		submitName.setEnabled(false);

		playersNumberMainPanel.add(submitName);

		//Aggiungo il listener della JTextField name (controllo che non sia vuota).
		name.addCaretListener(new CaretListener(){

			@Override
			public void caretUpdate(CaretEvent e) {
				if ("".equals(name.getText())){
					submitName.setEnabled(false);
				} else {
					submitName.setEnabled(true);
				}
			}

		});

		//Aggiungo il listener del bottone submitNames (Freccia di legno "Vai").
		submitName.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e){

				if(networkFlag){
					//Scelto il numero di giocatori e inserito il nome del server avvio il processo Server
					MainServer mainServer = new MainServer(Integer.parseInt((String)comboPlayers.getSelectedItem()), name.getText(), Integer.parseInt(port.getText()));
					mainServer.avviaServer();
					playersNumberScreen.dispose();
				}else{
					playersNumberScreen.dispose();
					try {

						new Thread(new WaitScreen(Inet4Address.getLocalHost().getHostAddress(), Integer.parseInt(port.getText()), 10000)).start();
						new Thread(new Runnable(){

							@Override
							public void run() {
								try {
									new MyServerSocket(Integer.parseInt((String)comboPlayers.getSelectedItem())).avviaServerSocket(name.getText(), Integer.parseInt(port.getText()));
								} catch (NumberFormatException
										| ClassNotFoundException | IOException e) {
									Main.LOGGER.log(Level.SEVERE, "Errore", e);
								}
							}
						}).start();

					} catch (NumberFormatException | IOException e1) {
						Main.LOGGER.log(Level.SEVERE, "Errore", e1);
					}
				}
			}
		});
	}

	@Override
	public void run() {
		playersNumberScreen.setVisible(true);
	}
}
