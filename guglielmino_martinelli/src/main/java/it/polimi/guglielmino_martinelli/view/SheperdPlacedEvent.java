package it.polimi.guglielmino_martinelli.view;

import it.polimi.guglielmino_martinelli.Coordinates;
import it.polimi.guglielmino_martinelli.events.GenericEvent;

/**
 * Evento generato dopo il posizionamento di un pastore.
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public class SheperdPlacedEvent extends GenericEvent {

	private static final long serialVersionUID = -412985008372298550L;

	private Sheperd sheperd;

	private Coordinates xy;

	public SheperdPlacedEvent(Sheperd sheperd, Coordinates xy) {
		super(EventType.SHEPERD_PLACED_EVENT);
		this.sheperd = sheperd;
		this.xy = xy;
	}

	public Sheperd getSheperd() {
		return sheperd;
	}

	public Coordinates getXy() {
		return xy;
	}
}