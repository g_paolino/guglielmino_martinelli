package it.polimi.guglielmino_martinelli.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/**
 * Finestra per la scelta del ruolo che si intende assumere, server o client.
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public class RoleView implements Runnable {
	
	/**
	 * Finestra principale.
	 */
	private JFrame roleScreen;
	
	public RoleView(final boolean networkRmi){
		
		//Creo la finestra roleScreen (JFrame).
		roleScreen = new JFrame("Sheepland");
		roleScreen.setSize(900, 700);
		roleScreen.setResizable(false);
		roleScreen.setLocationRelativeTo(null);
		roleScreen.setVisible(true);
		roleScreen.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		//Creo lo roleMainPanel con lo sfondo.
		JPanel roleMainPanel = new BackgroundPanel(this.getClass().getResource("/img/SHEEPLAND_home.png"));
		roleMainPanel.setLayout(null);
		
		roleScreen.add(roleMainPanel);
		
		
		//Scelta del ruolo nel gioco.
		
		JLabel instruction = new JLabel("In che ruolo vuoi giocare?");
		instruction.setBounds(0, 200, 900, 20);
		instruction.setHorizontalAlignment(SwingConstants.CENTER);
		instruction.setFont(new Font(null, Font.BOLD, 15));
		Color c = new Color(121,58,22);
		instruction.setForeground(c);
		
		roleMainPanel.add(instruction);
		
		//Creo i bottoni per scegliere il ruolo
		ImageIcon serverImg = new ImageIcon(this.getClass().getResource("/img/server.png"));
		JButton server = new JButton(serverImg);
		server.setBorder(null);
		server.setContentAreaFilled(false);
		
		ImageIcon clientImg = new ImageIcon(this.getClass().getResource("/img/client.png"));
		JButton client = new JButton(clientImg);
		client.setBorder(null);
		client.setContentAreaFilled(false);

		server.setBounds(200, 300, serverImg.getIconWidth(), serverImg.getIconHeight());
		client.setBounds(500, 300, clientImg.getIconWidth(), clientImg.getIconHeight());
		
		roleMainPanel.add(server);
		roleMainPanel.add(client);
		
		//Aggiungo il listener del bottone per la modalità server.
		server.addActionListener(new ActionListener(){
			
			@Override
			public void actionPerformed(ActionEvent e){
				roleScreen.dispose();
				//Se scelgo di essere server si apre la schermata dove impostare il numero di giocatori e il nome del giocatore server.
				PlayersNumberView pnv = new PlayersNumberView(networkRmi);
				new Thread(pnv).start();
			}
		});
		
		//Aggiungo il listener del bottone per la modalità client.
		client.addActionListener(new ActionListener(){
			
			@Override
			public void actionPerformed(ActionEvent e){
				roleScreen.dispose();
				//Se scelgo di essere client si apre la schermata per inserire i dati per la connessione e il nome del giocatore.
				new Thread(new PlayerInfoView(networkRmi)).start();
			}
		});
	}

	@Override
	public void run() {
		roleScreen.setVisible(true);
	}
}
