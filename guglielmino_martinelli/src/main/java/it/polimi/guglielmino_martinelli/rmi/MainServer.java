package it.polimi.guglielmino_martinelli.rmi;

import it.polimi.guglielmino_martinelli.Main;
import it.polimi.guglielmino_martinelli.view.ViewImplementationRmi;

import java.net.Inet4Address;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.util.logging.Level;

/**
 * Classe usata per lanciare il processo Server.
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public class MainServer {

	/**
	 * Numero massimo di giocatori.
	 */
	private int numberOfPlayers;
	/**
	 * Numero di porta del server.
	 */
	private int port;
	/**
	 * Nome del giocatore
	 */
	private String name;

	public MainServer(int numberOfPlayers, String name, int port){
		this.numberOfPlayers = numberOfPlayers;
		this.name = name;
		this.port = port;
	}

	/**
	 * Procedura di avvio del processo server.
	 */
	public void avviaServer(){	

		try {
			//Ottengo l'indirizzo ip locale (macchina server).
			String ip = Inet4Address.getLocalHost().getHostAddress();

			//Creo un registy sulla porta 8444.
			LocateRegistry.createRegistry(port);
			//Creo la falsa view del server.
			ViewImplementationRmi viewImplementation = new ViewImplementationRmi(numberOfPlayers);
			//Creo l'oggetto da esportare normalmente (in quanto la classe ServerImplementation estende UnicastRemoteObject).
			ServerImplementation serverImplementation = new ServerImplementation(ip, port, viewImplementation);

			viewImplementation.setServer(serverImplementation);

			//Aggiungo al registry l'associazione dell'oggetto serverImplementation con "//ip:port/Server".
			Naming.rebind("//"+ip+":"+port+"/Server", serverImplementation);

			//Lancio il processo client associato all'utente che fa da server.
			new MainClient(ip, port, name).avviaClient();
		} catch (MalformedURLException ex) {
			Main.LOGGER.log(Level.SEVERE, "Impossibile registrare l'oggetto indicato", ex);
		} catch (RemoteException ex) {
			Main.LOGGER.log(Level.SEVERE, "Errore di connessione", ex);
		} catch (UnknownHostException e) {
			Main.LOGGER.log(Level.SEVERE, "Unknown Host", e);
		}
	}
}