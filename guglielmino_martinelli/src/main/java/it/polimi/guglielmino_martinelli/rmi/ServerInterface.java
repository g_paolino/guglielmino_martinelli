package it.polimi.guglielmino_martinelli.rmi;

import it.polimi.guglielmino_martinelli.events.GenericEvent;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Interfaccia del server, sono elencati i metodi disponibili per i client remoti.
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public interface ServerInterface extends Remote {

	void addClient(ClientInterface client) throws RemoteException;

	int getIndex() throws RemoteException;

	int getNumberOfPlayers() throws RemoteException;

	String getPlayerName(int clientIndex) throws RemoteException;

	int getPlayerMoney(int clientIndex) throws RemoteException;

	void sendEventToViewImplementation(GenericEvent arg) throws RemoteException;

	void sendEventToClient(GenericEvent event, int clientIndex) throws RemoteException;

	void sendBroadcastEvent(GenericEvent event) throws RemoteException;
	
	String getIp() throws RemoteException;
	
	int getPort() throws RemoteException;
}