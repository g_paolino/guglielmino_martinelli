package it.polimi.guglielmino_martinelli.rmi;

import it.polimi.guglielmino_martinelli.Main;
import it.polimi.guglielmino_martinelli.events.GenericEvent;
import it.polimi.guglielmino_martinelli.view.GameView;
import it.polimi.guglielmino_martinelli.view.WaitScreen;

import java.rmi.RemoteException;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;

/**
 * Classe che implementa il client, la sua logica di conessione e i metodi
 * raggiungibili dalla rete.
 * 
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public class ClientImplementation implements ClientInterface, Observer {
	
	private WaitScreen waitView;
	/**
	 * Istanza della view di gioco.
	 */
	private GameView gameView;
	/**
	 * Istanza di ServerInterface, usata per chiamare i metodi remoti del server.
	 */
	private ServerInterface server;
	/**
	 * Indice di arrivo del client, viene settato dal Server.
	 */
	private int myIndex;
	
	public ClientImplementation(ServerInterface server){
		this.server = server;
		try {
			this.waitView = new WaitScreen(server.getIp(), server.getPort());
			new Thread(waitView).start();
		} catch (RemoteException e) {
			Main.LOGGER.log(Level.SEVERE, "Remote Exception", e);
		}
	}
	
	@Override
	public void closeWaitView() throws RemoteException {
		new Thread(new Runnable(){

			@Override
			public void run() {
				waitView.waitEnded();
			}
		}).start();
	}
	
	/**
	 * Mostra la view di gioco a questo client.
	 */
	@Override
	public void showGameView() throws RemoteException {
		//Creo la view di gioco.
		gameView = new GameView();
		//Dico che questa istanza di client implementation osserva la view appena creata.
		gameView.addObserver(this);
		new Thread(gameView).start();
	}
	
	/**
	 * Setta l'indice di questo client, viene chiamata dal server dopo che lo ha stabilito.
	 */
	@Override
	public void setMyIndex(int myIndex) throws RemoteException{
		this.myIndex = myIndex;
	}
	
	@Override
	public int getMyIndex() throws RemoteException {
		return myIndex;
	}
	
	/**
	 * Usato per inviare a questo client l'evento generato, questo client lo passa alla sua View.
	 */
	@Override
	public void sendEvent(GenericEvent event) throws RemoteException {
		gameView.update(null, event);
	}
	
	@Override
	public void update(Observable o, Object arg) {
		try {
			/* Arriva un evento dalla view di questo client, setta l'indice di questo client
			 * nell'evento e lo inoltra al server.
			 */
			((GenericEvent)arg).setPlayerIndex(myIndex);
			server.sendEventToViewImplementation((GenericEvent)arg);
		} catch (RemoteException e) {
			Main.LOGGER.log(Level.SEVERE, "Server non raggiungibile", e);
		}
	}
}