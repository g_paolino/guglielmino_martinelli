package it.polimi.guglielmino_martinelli.rmi;

import it.polimi.guglielmino_martinelli.events.GenericEvent;
import it.polimi.guglielmino_martinelli.view.ViewImplementationRmi;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

/**
 * Classe che implementa il server, la sua logica di connessione e i metodi
 * raggiungibili dalla rete.
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public class ServerImplementation extends UnicastRemoteObject implements ServerInterface, Observer {

	private static final long serialVersionUID = 677315917509350953L;

	/**
	 * HashMap usata per memorizzare i client connessi con il loro indice identificativo.
	 */
	private Map<Integer, ClientInterface> clients = new HashMap<Integer, ClientInterface>();
	/**
	 * Indice intero che verrà incrementato di 1 e assegnato ad ogni Client che si connette.
	 */
	private int index;
	/**
	 * Indirizzo ip del server.
	 */
	private String ip;
	/**
	 * Numero di port del server.
	 */
	private int port;
	/**
	 * Falsa view tramite la quale il server comunica con model e controller.
	 */
	private ViewImplementationRmi viewImplementation;

	protected ServerImplementation(String ip, int port, ViewImplementationRmi viewImplementation) throws RemoteException {
		super(0);
		this.viewImplementation = viewImplementation;
		this.index = 1;
		this.ip = ip;
		this.port = port;
	}

	/**
	 * Procedura che da l'ordine di mostrare la view di gioco ai client.
	 * @throws RemoteException
	 */
	public void goClients() throws RemoteException {
		for(ClientInterface client : clients.values()){
			client.closeWaitView();
			client.showGameView();
		}
	}

	/**
	 * Invio dell'evento alla View Implementation (falsa view).
	 */
	@Override
	public void sendEventToViewImplementation(GenericEvent event) throws RemoteException{
		viewImplementation.sendEvent(event);
	}

	/**
	 * Invio dell'evento ad un determinato client.
	 */
	@Override
	public void sendEventToClient(GenericEvent event, int clientIndex) throws RemoteException{
		for(ClientInterface client : clients.values()){
			if(client.getMyIndex() == clientIndex){
				client.sendEvent(event);
			}
		}
	}

	/**
	 * Invio dell'evento a tutti i client.
	 */
	@Override
	public void sendBroadcastEvent(GenericEvent event) throws RemoteException {
		for(ClientInterface client : clients.values()){
			client.sendEvent(event);
		}
	}

	/**
	 * Procedura chiamata quando un client chiede al server di essere aggiunto alla lista dei client connessi.
	 */
	@Override
	public void addClient(ClientInterface client) throws RemoteException {
		//Aggiungo il client con il suo indice caratteristico nella HashMap di client.
		clients.put(index, client);
		//Il server assegna al client il suo indice univoco.
		client.setMyIndex(index);
		//Incremento l'indice da assegnare al prossimo client.
		index++;
	}

	@Override
	public int getIndex() throws RemoteException {
		return index;
	}

	@Override
	public int getNumberOfPlayers() throws RemoteException {
		return viewImplementation.getNumberOfPlayers();
	}

	@Override
	public String getPlayerName(int clientIndex) throws RemoteException {
		return null;
	}

	@Override
	public int getPlayerMoney(int clientIndex) throws RemoteException {
		return 0;
	}

	@Override
	public void update(Observable o, Object arg) {
	}

	@Override
	public String getIp() throws RemoteException {
		return ip;
	}

	@Override
	public int getPort() throws RemoteException {
		return port;
	}
}