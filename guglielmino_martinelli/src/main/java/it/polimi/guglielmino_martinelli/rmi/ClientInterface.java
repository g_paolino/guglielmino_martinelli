package it.polimi.guglielmino_martinelli.rmi;

import it.polimi.guglielmino_martinelli.events.GenericEvent;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Interfaccia del client, sono elencati i metodi disponibili per il server remoto.
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public interface ClientInterface extends Remote {
	
	void closeWaitView() throws RemoteException;
	
	void showGameView() throws RemoteException;
	
	void setMyIndex(int myIndex) throws RemoteException;
	
	int getMyIndex() throws RemoteException;
	
	void sendEvent (GenericEvent event) throws RemoteException;
}