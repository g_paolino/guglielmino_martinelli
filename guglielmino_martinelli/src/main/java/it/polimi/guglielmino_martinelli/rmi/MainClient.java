package it.polimi.guglielmino_martinelli.rmi;

import it.polimi.guglielmino_martinelli.ConnectionLimitExceededException;
import it.polimi.guglielmino_martinelli.Main;
import it.polimi.guglielmino_martinelli.events.GenericEvent;
import it.polimi.guglielmino_martinelli.events.PlayerNameInsertedEvent;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.Level;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 * Classe usata per lanciare il processo client.
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public class MainClient {
	
	/**
	 * Indirizzo ip del server.
	 */
	private String ipServer;
	/**
	 * Numero di porta del server.
	 */
	private int portServer;
	/**
	 * Nome del giocatore.
	 */
	private String name;
	
	public MainClient(String ipServer, int portServer, String name){
		this.ipServer = ipServer;
		this.portServer = portServer;
		this.name = name;
	}
	
	/**
	 * Procedura di avviamento del processo client.
	 */
	public void avviaClient() {
		
		try {
			//Ottengo il riferimento remoto associato alla stringa passata (contiene l'host target e l'identificativo dell'oggetto sull'host).
			ServerInterface server = (ServerInterface)Naming.lookup("//"+ipServer+":"+portServer+"/Server");
			
			//Controllo, istanzio il client solo se ci sono ancora posti come giocatore disponibili.
			if(server.getIndex() <= server.getNumberOfPlayers()){
				
				//Genero l'evento nuovo giocatore inserito.
				GenericEvent event = new PlayerNameInsertedEvent(name);
				event.setPlayerIndex(server.getIndex());
				//Creo l'oggetto client normalmente.
				ClientImplementation client = new ClientImplementation(server);
				/*Tuttavia, dato che ClientImplementation non estende la classe UnicastRemoteObject, devo creare un riferimento remoto
				all'oggetto col metdo UnicastRemoteObject.exportObject che prende come parametri l'oggetto da esportare e la porta da
				utilizzare per la connessione. Con 0 la porta viene scelta automaticamente.
				Altrimenti avrebbe tentato di serializzare l'oggetto e di passarlo come copia al server.
				In questo caso non devo associare un identificativo all'oggetto (in quanto il riferimento remoto verrà passato al server).*/
				ClientInterface remoteRef = (ClientInterface) UnicastRemoteObject.exportObject(client, 0);
				server.addClient(remoteRef);
				//Passo al server l'evento nuovo gicatore inserito.
				server.sendEventToViewImplementation(event);	
			} else {
				//Eccezione lanciata in caso di connessione di più client del previsto.
				throw new ConnectionLimitExceededException();
			}
		} catch (MalformedURLException e) {
			Main.LOGGER.log(Level.SEVERE, "URL non trovato", e);
		} catch (RemoteException e) {
			Main.LOGGER.log(Level.SEVERE, "Errore di connessione", e);
			JFrame errorFrame = new JFrame();
			errorFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			errorFrame.setLocationRelativeTo(null);
			JOptionPane.showMessageDialog(errorFrame,"Server non trovato!","Errore",JOptionPane.ERROR_MESSAGE, new ImageIcon(this.getClass().getResource("/img/error.png")));
			errorFrame.dispose();
		} catch (NotBoundException e) {
			Main.LOGGER.log(Level.SEVERE, "Il riferimento passato non è associato a nulla", e);
		} catch (ConnectionLimitExceededException e) {
			JFrame errorFrame = new JFrame();
			errorFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			JOptionPane.showMessageDialog(errorFrame,"Numero connessioni massime raggiunte!","Errore",JOptionPane.ERROR_MESSAGE, new ImageIcon(this.getClass().getResource("/img/error.png")));
			errorFrame.dispose();
		}
	}
}