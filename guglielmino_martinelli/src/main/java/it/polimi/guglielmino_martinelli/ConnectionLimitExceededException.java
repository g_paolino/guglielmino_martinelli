package it.polimi.guglielmino_martinelli;

/**
 * Eccezione che viene lanciata quando si connettono più client del limite massimo previsto dal server.
 * @author Paolo Guglielmino
 * @author Simone Martinelli
 */
public class ConnectionLimitExceededException extends Exception {

	private static final long serialVersionUID = 4736159920348832947L;

}