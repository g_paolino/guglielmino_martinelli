package guglielmino_martinelli;

import static org.junit.Assert.assertEquals;
import it.polimi.guglielmino_martinelli.controller.GameManager;
import it.polimi.guglielmino_martinelli.events.PlayersNamesInsertedEvent;
import it.polimi.guglielmino_martinelli.model.GameStatus;
import it.polimi.guglielmino_martinelli.model.Player;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestGameManager {

	private GameManager gm;
	
	private GameStatus gs;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception{
		System.out.println("Inizio del test " + System.currentTimeMillis());
	}
	
	@AfterClass
	public static void setUpAfterClass() throws Exception{
		System.out.println("Fine del test " + System.currentTimeMillis());
	}
	
	@Before
	public void setUp(){
		System.out.println("Inizializzo ambiente per il nuovo test");
		gs = new GameStatus();
		gs.setNumberOfPlayers(4);
		gm = new GameManager(gs, null);
	}
	
	@After
	public void tearDown(){
		System.out.println("Fine test");
	}
	
	@Test
	public void testInsertPlayer() throws Exception{
		Class<?>[] parameterTypes = new Class[2];
		parameterTypes[0] = int.class;
		parameterTypes[1] = String.class;
		
		Method met = gm.getClass().getDeclaredMethod("insertPlayer", parameterTypes);
		met.setAccessible(true);
		
		Object [] parameters = new Object[2];
		parameters[0] = 1;
		parameters[1] = "Paolo";
		met.invoke(gm, parameters);
		
		Player p = new Player("Paolo", 20, 1);
	
		assertEquals("Test sull'inserimento di un giocatore", p.getIndex(), gs.getPlayers().get(1).getIndex());
		assertEquals("Test sull'inserimento di un giocatore", p.getName(), gs.getPlayers().get(1).getName());
		assertEquals("Test sull'inserimento di un giocatore", p.getMoney(), gs.getPlayers().get(1).getMoney());
		assertEquals("Test sull'inserimento di un giocatore", p.getCardsOwned(), gs.getPlayers().get(1).getCardsOwned());
		assertEquals("Test sull'inserimento di un giocatore", p.getPosition(), gs.getPlayers().get(1).getPosition());
	}
	
	@Test
	public void testInsertPlayers() throws Exception{
		Class<?>[] parameterTypes = new Class[1];
		parameterTypes[0] = PlayersNamesInsertedEvent.class;
		
		Method met = gm.getClass().getDeclaredMethod("insertPlayers", parameterTypes);
		met.setAccessible(true);
		
		List<String> names = new ArrayList<String>();
		names.add("Paolo");
		names.add("Simone");
		
		PlayersNamesInsertedEvent event = new PlayersNamesInsertedEvent(names);
		
		Object [] parameters = new Object[1];
		parameters[0] = event;
		met.invoke(gm, parameters);
		
		assertEquals("Test sull'inserimento dei giocatori", names.get(0), gs.getPlayers().get(1).getName());
		assertEquals("Test sull'inserimento dei giocatori", names.get(1), gs.getPlayers().get(2).getName());
		assertEquals("Test sull'inserimento dei giocatori", 30, gs.getPlayers().get(1).getMoney());
		assertEquals("Test sull'inserimento dei giocatori", 30, gs.getPlayers().get(2).getMoney());	
	}
	
	@Test
	public void testBlackSheepEscapes() throws Exception{
		
		gs.getGameMap().createGraph();
		
		Class<?>[] parameterTypes = new Class[0];
		
		Method met = gm.getClass().getDeclaredMethod("blackSheepEscapes", parameterTypes);
		met.setAccessible(true);
		
		Object [] parameters = new Object[0];
		met.invoke(gm, parameters);
		
		assertEquals("Test sulla fuga della pecora nera, controllo che non è più a Sheepsburg", false, gs.getGameMap().getAllRegions().get(""+19).getBlackSheep());
	}
	
	@Test
	public void testSheperdMoved() throws Exception{
		gs.getGameMap().createGraph();
		
		Class<?>[] parameterTypes = new Class[0];
		
		Method met = gm.getClass().getDeclaredMethod("sheperdMoved", parameterTypes);
		met.setAccessible(true);
		
		Object [] parameters = new Object[0];
		met.invoke(gm, parameters);
		
		
		assertEquals("Test sul metodo sheperdMoved", 2, gs.getActionsLeft());
		assertEquals("Test sul metodo sheperdMoved", true, gs.canMoveSheep());
		assertEquals("Test sul metodo sheperdMoved", true, gs.canBuyCard());
	}
	
	@Test
	public void testSheepMoved() throws Exception{
		gs.getGameMap().createGraph();
		
		Class<?>[] parameterTypes = new Class[0];
		
		Method met = gm.getClass().getDeclaredMethod("sheepMoved", parameterTypes);
		met.setAccessible(true);
		
		Object [] parameters = new Object[0];
		met.invoke(gm, parameters);
		
		
		assertEquals("Test sul metodo sheepMoved", 2, gs.getActionsLeft());
		assertEquals("Test sul metodo sheepMoved", false, gs.canMoveSheep());
		
	}
	
	@Test
	public void testFromRgbToRegion() throws Exception{
		gs.getGameMap().createGraph();
		
		Class<?>[] parameterTypes = new Class[1];
		parameterTypes[0] = int.class;
		
		Method met = gm.getClass().getDeclaredMethod("fromRgbToRegion", parameterTypes);
		met.setAccessible(true);
		
		Object [] parameters = new Object[1];
		parameters [0] = 190;
		
		assertEquals("Test sulla conversione da RGB alla regione", gs.getGameMap().getAllRegions().get(""+6), met.invoke(gm, parameters));
	}
	
	@Test
	public void testCardBuyed() throws Exception{
		gs.getGameMap().createGraph();
		
		Class<?>[] parameterTypes = new Class[0];
		
		Method met = gm.getClass().getDeclaredMethod("cardBuyed", parameterTypes);
		met.setAccessible(true);
		
		Object [] parameters = new Object[0];
		met.invoke(gm, parameters);
		
		
		assertEquals("Test sul metodo sheepMoved", 2, gs.getActionsLeft());
		assertEquals("Test sul metodo sheepMoved", false, gs.canBuyCard());
	}
}
