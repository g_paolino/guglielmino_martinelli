package guglielmino_martinelli;

import static org.junit.Assert.assertEquals;
import it.polimi.guglielmino_martinelli.Coordinates;
import it.polimi.guglielmino_martinelli.model.GameMap;
import it.polimi.guglielmino_martinelli.model.GameStatus;
import it.polimi.guglielmino_martinelli.model.LandType;
import it.polimi.guglielmino_martinelli.model.Player;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestGameStatus {
	
	private GameStatus status;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception{
		System.out.println("Inizio del test " + System.currentTimeMillis());
	}
	
	@AfterClass
	public static void setUpAfterClass() throws Exception{
		System.out.println("Fine del test " + System.currentTimeMillis());
	}
	
	@Before
	public void setUp(){
		System.out.println("Inizializzo ambiente per il nuovo test");
		status = new GameStatus();
	}
	
	@After
	public void tearDown(){
		System.out.println("Fine test");
	}
	
	@Test
	public void testCostruttoreGameStatus(){
		GameStatus statusTest = new GameStatus();
		
		GameMap gameMap = new GameMap();
		Map<LandType, Integer> cardsCurrentCost = new HashMap<LandType, Integer>();
		Map<Integer, Player> players = new HashMap<Integer, Player>();
		
		assertEquals("Test sulla mappa, grafo", gameMap.getGraph(), statusTest.getGameMap().getGraph());
		assertEquals("Test sulla mappa, strade", gameMap.getAllRoads(), statusTest.getGameMap().getAllRoads());
		assertEquals("Test sulla mappa, regioni", gameMap.getAllRegions(), statusTest.getGameMap().getAllRegions());
		assertEquals("Test sul costo delle tessere terreno", cardsCurrentCost, statusTest.getCardsCurrentCost());
		assertEquals("Test sui recinti", 32, statusTest.getFencesAvailable());
		assertEquals("Test sui giocatori", players, statusTest.getPlayers());
		assertEquals("Test sulla fase finale", false, statusTest.isFinalPhase());
		assertEquals("Test sull'indice del pastore", 1, statusTest.getCurrentSheperdIndex());
		assertEquals("Test sulle azioni rimaste", 3, statusTest.getActionsLeft());
		assertEquals("Test sulla selezione del pastore", false, statusTest.isSheperdSelected());
		assertEquals("Test sul muovi pecora", true, statusTest.canMoveSheep());
		assertEquals("Test sul compra tessera", true, statusTest.canBuyCard());
	}
	
	@Test
	public void testNextPlayerIndex() throws Exception{
		status.setNumberOfPlayers(4);
		status.setCurrentPlayerIndex(1);
		
		Class<?>[] parameterTypes = new Class[0];
		
		Method met = status.getClass().getDeclaredMethod("nextPlayerIndex", parameterTypes);
		met.setAccessible(true);
		
		Object [] parameters = new Object[0];
		met.invoke(status, parameters);
		
		assertEquals("Test sul passaggio al giocatore successivo, caso corrente 1", 2, status.getCurrentPlayerIndex());
		
		status.setCurrentPlayerIndex(4);
		met.invoke(status, parameters);
		
		assertEquals("Test sul passaggio al giocatore successivo, caso corrente 4", 1, status.getCurrentPlayerIndex());
	}
	
	@Test
	public void testInsertPlayer(){
		
		Player p = new Player("Simone", 20, 2);
		status.insertPlayer(2, p);
		
		assertEquals("Test sull'inserimento di un giocatore, indice", p.getIndex(), status.getPlayers().get(2).getIndex());
		assertEquals("Test sull'inserimento di un giocatore, nome", p.getName(), status.getPlayers().get(2).getName());
		assertEquals("Test sull'inserimento di un giocatore, danari", p.getMoney(), status.getPlayers().get(2).getMoney());
		assertEquals("Test sull'inserimento di un giocatore, tessere che possiede", p.getCardsOwned(), status.getPlayers().get(2).getCardsOwned());
		assertEquals("Test sull'inserimento di un giocatore, posizione", p.getPosition(), status.getPlayers().get(2).getPosition());
	}
	
	@Test
	public void testAssignCardToPlayer(){
		Player p = new Player("Simone", 20, 1);
		status.insertPlayer(1, p);
		
		status.assignCardToPlayer(LandType.FIELD, 1);
		
		assertEquals("Test sull'assegnamento di una tessera terreno", 1, (int)status.getPlayers().get(1).getCardsOwned().get(LandType.FIELD));
	}
	
	@Test
	public void testMoneyPerPlayer(){
		status.setNumberOfPlayers(3);
		assertEquals("Test sui danari per giocatore, caso 3 giocatori", 20, status.moneyPerPlayer());
		
		status.setNumberOfPlayers(2);
		assertEquals("Test sui danari per giocatore, caso 2 giocatori", 30, status.moneyPerPlayer());
	}
	
	@Test
	public void testSetInitialCardsCosts(){
		status.setInitialCardsCosts();
		
		assertEquals("Test sull'assegnamento del costo iniziale alle tessere terreno, field", 0, (int)status.getCardsCurrentCost().get(LandType.FIELD));
		assertEquals("Test sull'assegnamento del costo iniziale alle tessere terreno, forest", 0, (int)status.getCardsCurrentCost().get(LandType.FOREST));
		assertEquals("Test sull'assegnamento del costo iniziale alle tessere terreno, heath", 0, (int)status.getCardsCurrentCost().get(LandType.HEATH));
		assertEquals("Test sull'assegnamento del costo iniziale alle tessere terreno, lake", 0, (int)status.getCardsCurrentCost().get(LandType.LAKE));
		assertEquals("Test sull'assegnamento del costo iniziale alle tessere terreno, mountain", 0, (int)status.getCardsCurrentCost().get(LandType.MOUNTAIN));
		assertEquals("Test sull'assegnamento del costo iniziale alle tessere terreno, plain", 0, (int)status.getCardsCurrentCost().get(LandType.PLAIN));
	}
	
	@Test
	public void testUpdateCardsCurrentCost(){
		status.setInitialCardsCosts();
		status.updateCardsCurrentCost(LandType.FOREST);
		
		assertEquals("Test sull'aggiornamento del costo delle tessere terreno, forest", 1, (int)status.getCardsCurrentCost().get(LandType.FOREST));
	}
	
	@Test
	public void testSetFencesAvailable(){
		status.setFencesAvailable(13, new Coordinates(0,0)); //imposto coordinata 00 solo perchè richiesta
		
		assertEquals("Test sui recinti disponibili, caso maggiori di 12", 13, status.getFencesAvailable());
		
		
		status.setFencesAvailable(11, new Coordinates(0,0)); //in questo settaggio avviene il passaggio a true della fase finale
		status.setFencesAvailable(10, new Coordinates(0,0)); 
		
		assertEquals("Test sui recinti disponibili", 10, status.getFencesAvailable());
		assertEquals("Test sulla fase finale", true, status.isFinalPhase());

	}
	
	@Test
	public void testSetActionLeft(){
		status.setActionsLeft(2);
		
		assertEquals("Test sulle azioni rimaste", 2, status.getActionsLeft());
		
		status.setActionsLeft(0);
		
		assertEquals("Test sulle azioni rimaste", 3, status.getActionsLeft());
		assertEquals("Test sulla selezione del pastore", false, status.isSheperdSelected());
		assertEquals("Test sul muovi pecora", true, status.canMoveSheep());
		assertEquals("Test sul compra tessera", true, status.canBuyCard());
		
	}
	
}
