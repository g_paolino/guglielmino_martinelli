package guglielmino_martinelli;

import static org.junit.Assert.assertEquals;
import it.polimi.guglielmino_martinelli.Coordinates;
import it.polimi.guglielmino_martinelli.model.GameMap;
import it.polimi.guglielmino_martinelli.model.LandType;
import it.polimi.guglielmino_martinelli.model.Region;
import it.polimi.guglielmino_martinelli.model.Road;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestGameMap {

	private GameMap map;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception{
		System.out.println("Inizio del test " + System.currentTimeMillis());
	}
	
	@AfterClass
	public static void setUpAfterClass() throws Exception{
		System.out.println("Fine del test " + System.currentTimeMillis());
	}
	
	@Before
	public void setUp(){
		System.out.println("Inizializzo ambiente per il nuovo test");
		map = new GameMap();
	}
	
	@After
	public void tearDown(){
		System.out.println("Fine test");
	}
	
	@Test
	public void testCostruttoreGameMap(){
		
		GameMap mapTest = new GameMap();
		Map<String, Road> allRoadsTest = new HashMap<String, Road>();
		Map<String, Region> allRegionsTest = new HashMap<String, Region>();
		
		assertEquals("Test sulle strade", allRoadsTest, mapTest.getAllRoads());
		assertEquals("Test sulle regioni", allRegionsTest, mapTest.getAllRegions());
	}
	
	@Test
	public void testCreateGraph(){
		map.createGraph();
		
		Road roa = new Road();
		roa.setNumber(6);
		roa.setXY(new Coordinates(228, 164));
		
		Region reg = new Region();
		reg.initRegion();
		reg.setType(LandType.FIELD);
		reg.setRGB(242);
		reg.setXY(new Coordinates(168, 148));
		
		assertEquals("Test sulla strada, numero", roa.getNumber(), map.getAllRoads().get(""+5).getNumber());
		assertEquals("Test sulla strada, coordinate", roa.getXY(), map.getAllRoads().get(""+5).getXY());
		
		assertEquals("Test sulla regione, pecora nera", reg.getBlackSheep(), map.getAllRegions().get(""+2).getBlackSheep());
		assertEquals("Test sulla regione, numero percore bianche", reg.getWhiteSheep(), map.getAllRegions().get(""+2).getWhiteSheep());
		assertEquals("Test sulla regione, tipo di terreno", reg.getType(), map.getAllRegions().get(""+2).getType());
		assertEquals("Test sulla regione, codice RGB", reg.getRGB(), map.getAllRegions().get(""+2).getRGB());
		assertEquals("Test sulla regione, coordinate", reg.getXY(), map.getAllRegions().get(""+2).getXY());
		
		assertEquals("Test sul collegamento road to road", true, map.getGraph().containsEdge(map.getAllRoads().get(""+2), map.getAllRoads().get(""+7)));
		assertEquals("Test sul collegamento region to road", true, map.getGraph().containsEdge(map.getAllRegions().get(""+3), map.getAllRoads().get(""+6)));
	}
	
	@Test
	public void testGetAdjacentRoads(){
		map.createGraph();
		
		Set<Road> setOfRoads1 = new HashSet<Road>();
		setOfRoads1.add(map.getAllRoads().get(""+4));
		setOfRoads1.add(map.getAllRoads().get(""+10));
		
		Set<Road> setOfRoads2 = new HashSet<Road>();
		setOfRoads2.add(map.getAllRoads().get(""+9));
		setOfRoads2.add(map.getAllRoads().get(""+13));
		setOfRoads2.add(map.getAllRoads().get(""+14));
		
		assertEquals("Test sull'adiacenza road-roads", setOfRoads1, map.getAdjacentRoads(map.getAllRoads().get(""+3)));
		assertEquals("Test sull'adiacenza region-roads", setOfRoads2, map.getAdjacentRoads(map.getAllRegions().get(""+15)));
	}
	
	@Test
	public void testGetAdjacentRegions(){
		map.createGraph();
		Set<Region> setOfRegions = new HashSet<Region>();
		setOfRegions.add(map.getAllRegions().get(""+3));
		setOfRegions.add(map.getAllRegions().get(""+13));
		
		assertEquals("Test sull'adiacenza road-regions", setOfRegions, map.getAdjacentRegions(map.getAllRoads().get(""+6)));
	}
	
	@Test
	public void testGetCorrectDestination(){
		map.createGraph();
		assertEquals("Test sulla corretta destinazione", map.getAllRegions().get(""+7), map.getCorrectDestination(map.getAllRoads().get(""+20), map.getAllRegions().get(""+8)));
	}
	
	@Test
	public void testSheepPerLandType(){
		map.createGraph();
		Map<LandType, Integer> count = new HashMap<LandType, Integer>();
		count.put(LandType.FIELD, 3);
		count.put(LandType.FOREST, 3);
		count.put(LandType.HEATH, 3);
		count.put(LandType.LAKE, 3);
		count.put(LandType.MOUNTAIN, 3);
		count.put(LandType.PLAIN, 3);
		count.put(LandType.SHEEPSBURG, 0);
		assertEquals("Test sul numero di pecore per tipo di terreno", count, map.sheepPerLandType());
	}
	
	@Test
	public void findBlackSheep(){
		assertEquals("Test sulla ricerca della pecora nera", map.getAllRegions().get(""+19), map.findBlackSheepRegion());
	}
}