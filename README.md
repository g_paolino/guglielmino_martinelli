### SHEEPLAND ###

Project developed within the context of Software Engineering 1 (Politecnico di Milano A.A 2013/2014) as my bachelor thesis. 
SheepLand is a board game and our goal was to develop the electronic version using Java SE as programming language.

### How do I get set up? ###

- Unzip the file: guglielmino_martinelli/guglielmino_martinelli.zip
- Run the executable jar file and have fun!